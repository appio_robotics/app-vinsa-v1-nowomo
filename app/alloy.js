/////  CONFIG


var db = Ti.Database.open ('_alloy_');
db.execute ('DROP TABLE IF EXISTS incident;');
db.close ();

Alloy.Collections.incident = Alloy.createCollection('incident');
Alloy.Collections.user = Alloy.createCollection('user');

Alloy.Globals.searchParams = {};



Alloy.setActionbar = function(thisWindow, title, notLogo) {
	var activity = thisWindow.activity;
	activity.actionBar.title = title;
	activity.actionBar.displayHomeAsUp = false;
	var logoUrl = notLogo ? '/images/transparent.png' : '/images/back.png';
	activity.actionBar.logo = logoUrl;

	activity.actionBar.onHomeIconItemSelected = function() {
		thisWindow.close();
	};

	activity.onCreateOptionsMenu = function(e) {

		// Logout button
		btnLogout = e.menu.add({
			title : "Salir",
			showAsAction : Ti.Android.SHOW_AS_ACTION_ALWAYS
		});
		btnLogout.addEventListener("click", function(e) {
			require('/loginManager').logout();
		});
	};
};
