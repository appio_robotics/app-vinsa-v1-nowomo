// Dev functions

exports.err = function(e) {
	if (e && e.message) {
		exports.inf(e);

	} else {
		//alert("in dev.err:  An error has ocurred, but not propery e was received! ")
	}
};

exports.inf = function() {
	for (var i = 0, l = arguments.length; i < l; i++) {
		var argument = arguments[i];
		if ( argument instanceof Array) {
			Ti.API.info("Array: ");
			var text = "";
			for (var j = 0, l2 = argument.length; j < l2; j++) {
				text += argument[j] + ", ";
			}
			Ti.API.info(text);
		}

		Ti.API.info(argument);
	}
};

exports.enumProps = function() {
	var title, object;
	if (arguments.length !== 1) {
		title = arguments[0];
		object = arguments[1];
	} else {
		title = "Enumeración de propiedades";
		object = arguments[0];
	}

	exports.inf(title);

	for (var prop in object) {
		var msg = prop + ": " + object[prop];
		exports.inf(msg);

	}
};
// DANGER!!!  Removes ALL properties, not just our properties!
exports.clearAllProperties = function() {
	var props = Ti.App.Properties.listProperties();

	for (var i = 0, ilen = props.length; i < ilen; i++) {
		var value = Ti.App.Properties.getString(props[i]);
		
		exports.inf("property " + props[i] + " con valor " + value);
		
		Ti.App.Properties.removeProperty(props[i]);
	}
};

exports.isDev = false;
