var empty = {};
function aux_mixin(/*Object*/target, /*Object*/source) {
	var name, s, i;
	for (name in source) {
		s = source[name];
		if (!( name in target) || (target[name] !== s && (!( name in empty) || empty[name] !== s))) {
			target[name] = s;
		}
	}
	return target;
	// Object
};

function mixin(/*Object*/obj, /*Object...*/props) {
	if (!obj) {
		obj = {};
	}
	for (var i = 1, l = arguments.length; i < l; i++) {
		aux_mixin(obj, arguments[i]);
	}
	return obj;
	// Object
};

//create a new object, DEEP combining the properties of the passed objects with the last arguments having
//priority over the first ones
function combine(/*Object*/obj1, /*Object...*/obj2) {
	
	for (var p in obj2) {
        if( obj2.hasOwnProperty(p)){
            obj1[p] = typeof obj2[p] === 'object' ? combine(obj1[p], obj2[p]) : obj2[p];
        }
    }
    return obj1;

};

exports.mixin = mixin;
exports.combine = combine;

exports.isAndroid = function() {
	return Ti.Platform.osname === "android" ? true : false;
};

exports.isSimulator = function() {
	if (Ti.Platform.model === 'Simulator' || Ti.Platform.model.indexOf('sdk') !== -1) {
		return true;
	} else {
		return false;
	}
};

exports.trim = function(str) {
    return str.replace(/^\s+|\s+$/g,"");
};

