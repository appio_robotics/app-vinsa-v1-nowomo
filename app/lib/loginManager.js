// Require modules
var Dev = require('/dev/Dev');
var Utils = require('/Utils');

var DataModel = require('/DataModel');

exports.getUserDataIfRememberMe = function() {
	var currentUser = DataModel.currentUser.get();
	if ( typeof currentUser === 'undefined' || !currentUser) {
		return false;
	}

	var userData;
	if (currentUser['rememberMe']) {
		userData = currentUser;
	} else {
		userData = false;
	}

	return userData;
};

exports.isLogged = function(_args) {

	var isLogged; 

	var currentUser = DataModel.currentUser.get();
	
	if(currentUser && currentUser.isLogged){
		isLogged = true;
	}
	else{
		isLogged = false;
	}
	
	return isLogged;

};

exports.checkLogin = function(_args) {

	// Received params
	var userNameReceived = _args.user, passwordReceived = _args.psw, rememberMe = _args.rememberMe;

	var user = Alloy.Collections.user;
	// Fetch the user with that userName
	user.fetch({
		sql : {
			where : {
				userName : userNameReceived
			}
		},
		success : function(models) {
Dev.inf("recibo del server model " + JSON.stringify(models));
			var loginSuccess = false;

			if (models.length > 0) {
				var theUserModel = models.pop();
				var theUser = theUserModel.toJson();

				var md5Password = Titanium.Utils.md5HexDigest(passwordReceived + theUser['salt']);

				loginSuccess = md5Password === theUser['password'];

			}

			if (loginSuccess) {

				exports.login({
					// passwordInForm (before MD5 is added here so it can be used in the login form when rememberMe is true)
					currentUser : Utils.combine(theUser, {
						passwordInForm : passwordReceived
					}),
					rememberMe : rememberMe
				});

				if (_args.success) {
					_args.success();
				}

			} else {
				if (_args.error) {
					_args.error();
				}
			}

		},
		error : function(data) {
			alert("hay un error en el success");
			alert(data);
		}
	});

};

exports.login = function(_args) {

	var data = Utils.combine(_args.currentUser, {
		rememberMe : _args.rememberMe,
		isLogged : true
	});

	DataModel.currentUser.set(data);
	
	// Subscribe for push notifications
	require('/PushManager').subscribe();
};

exports.logout = function() {

	var currentUser = DataModel.currentUser.get();
	var data;

	if (currentUser['rememberMe']) {
		data = currentUser;
		data.isLogged = false;
	} else {
		data = false;
	}	

	DataModel.currentUser.set(data);

	// Open Login Window
	Alloy.createController('login').getView().open();

};
