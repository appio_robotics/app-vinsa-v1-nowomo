

var Cloud = require('/ti.cloud'), CloudPush = require('/ti.cloudpush');

var deviceToken, acsUser = "vinsa", acsPassword = "vinsa44xda73";

CloudPush.debug = true;
CloudPush.enabled = true;
CloudPush.showTrayNotificationsWhenFocused = true;
CloudPush.focusAppOnPush = false;
CloudPush.singleCallback = true;

exports.subscribe = function() {
	
	
	if (!Ti.Network.online) {
		return;
	}
	
	var currentUser = require('/DataModel').currentUser.get();

	var userId = currentUser['id'];
	Cloud.Users.login({
		login : acsUser,
		password : acsPassword
	}, function(e) {

		if (e.success) {
			CloudPush.retrieveDeviceToken({
				success : function deviceTokenSuccess(e) {
					deviceToken = e.deviceToken;

					// Unsubscribe from prevoius channels
					Cloud.PushNotifications.unsubscribe({
						device_token : deviceToken
					}, function(e) {
	
							// Subscribe for channel "user_" + userId
							Cloud.PushNotifications.subscribe({
								channel : 'user_' + userId, 
								device_token : deviceToken,
								type : 'android'
							}, function(e) {
								
								if (e.success) {
									CloudPush.addEventListener('callback', function(evt) {
										
									});

								} else {
									if (require('/dev/Dev').isDev) {
									alert('Error in subscribe:' + ((e.error && e.message) || JSON.stringify(e)));
									}
								}
							});						
					});

				},
				error : function deviceTokenError(e) {

					if (require('/dev/Dev').isDev) {
						alert('Failed to register for push! ' + e.error);
					}

				}
			});

		} else {
			if (require('/dev/Dev').isDev) {
				alert('Error en registro de push: ' + ((e.error && e.message) || JSON.stringify(e)));
			}

		}
	});
};

