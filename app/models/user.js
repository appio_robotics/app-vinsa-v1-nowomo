var env = "pre";
var url = "http://gestionvinsa.es/" + env + "/public/rest";


// APPIO !!!
//var url = "https://www.appio.es/apps/facebook/appiogestor/v01/public/rest";

exports.definition = {
	config : {

		"debug" : 1,

		"columns" : {
			"id" : "INTEGER",

			"nombre" : "TEXT",
			"apellido" : "TEXT",

			"role" : "TEXT",
			
			"userName": "TEXT",
			"password": "TEXT",
			"salt": "TEXT"
		},

		"URL" : url,

		"adapter" : {
			"type" : "sqlrest",
			"collection_name" : "user",
			"idAttribute" : "id"
		},

		// delete all models on fetch
		"deleteAllOnFetch" : true,
		
		"parentNode" : "users"
	},
	extendModel : function(Model) {
		_.extend(Model.prototype, {
			toJson : function() {
				var json = this.toJSON();
				return _.extend(json, {
					iconUrl : "/images/icons/circleGreen.png"
				});
			}
		});

		return Model;
	},

	extendCollection : function(Collection) {
		_.extend(Collection.prototype, {
			// Extend, override or implement Backbone.Collection
		});

		return Collection;
	}
};
