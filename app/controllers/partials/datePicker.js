var args = arguments[0] || {};

$.dateTextField.hintText = args.title;

$.datePickerContainer.addEventListener('click', function() {
	var picker = Ti.UI.createPicker({
		type : Ti.UI.PICKER_TYPE_DATE,
	});
	picker.showDatePickerDialog({
		callback : function(e) {
			if (!e.cancel) {
				var dateValue = e.value;
				$.dateTextField.value = (dateValue.getMonth() + 1) + '/' + dateValue.getDate() + '/' + dateValue.getFullYear();
				$.dateTextField.blur();
			}
		}
	});
});

