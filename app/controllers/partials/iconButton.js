var args = arguments[0] || {};

$.icon.image = "/images/icons/" + args.image;
$.label.text = args.title;

$.iconContainer.addEventListener('click', function(e){
	$.trigger('click', e);
});
