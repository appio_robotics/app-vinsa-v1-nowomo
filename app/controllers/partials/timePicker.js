var args = arguments[0] || {};

$.timeTextField.addEventListener('click', function() {
	var picker = Ti.UI.createPicker({
		type : Ti.UI.PICKER_TYPE_TIME
	});
	picker.showTimePickerDialog({
		callback : function(e) {
			var time = e.value;
			$.timeTextField.value = time.getHours() + ':' + time.getMinutes();
			$.timeTextField.blur();
		}
	});
}); 