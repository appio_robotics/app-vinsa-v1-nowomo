var args = arguments[0] || {};
var thisWindow = $.filter;

var initialized = false;

function init(){
	
	$.chkOpen.getView('checkboxElement').value = $.chkInProcess.getView('checkboxElement').value = true;
	
	$.startDate.getView('dateTextField').blur();
	
	$.startDate.getView('dateTextField').addEventListener('focus', function(){
	
			if(!initialized){
				$.startDate.getView('dateTextField').blur();
				initialized = true;
			}
			
		});
}

function search(){
	
	var abiertaValue = $.chkOpen.getView('checkboxElement').value || false;
	var cursoValue = $.chkInProcess.getView('checkboxElement').value || false;
	var cerradaValue = $.chkClose.getView('checkboxElement').value || false;
	
	var fechasSelected = $.chkDate.getView('checkboxElement').value || false;
	var fechaapertura = (fechasSelected && $.startDate.getView('dateTextField').value) || false;
	var fechacierre = (fechasSelected && $.endDate.getView('dateTextField').value) || false;
	
	var params = {
			abierta: abiertaValue,
			curso: cursoValue,
			cerrada: cerradaValue,
			fechasSelected: fechasSelected,
			fechaapertura: fechaapertura,
			fechacierre: fechacierre
	};
	
	Alloy.Globals.searchParams = params;
	
	Alloy.createController('incidentsList', {
		params:params
	}).getView().open();
}


function logout(e) {
	Alloy.createController('login').getView().open();
}


thisWindow.addEventListener('open', function(e) {
	init();
	Alloy.setActionbar(thisWindow, 'Menú');
});


