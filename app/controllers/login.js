var initialized = false;

var LoginManager = require('/loginManager');
var Dev = require('/dev/Dev');

function init() {
	$.user.blur();
	$.user.addEventListener('focus', function() {
		if (!initialized) {
			$.user.blur();
			initialized = true;
		}
	});
	
	var userData = LoginManager.getUserDataIfRememberMe();		
	
	if(userData){
		$.user.value = userData['userName'];
		$.psw.value = userData['passwordInForm'];
		$.rememberMe.getView('checkboxElement').value = userData['rememberMe'];
	}

}

function login(e) {
	
	var rememberMe = typeof $.rememberMe.getView('checkboxElement').value !== "undefined" ? $.rememberMe.getView('checkboxElement').value : false;

	LoginManager.checkLogin({
		user : $.user.value,
		psw : $.psw.value,
		rememberMe: rememberMe,
		success : function() {
			Alloy.createController('main').getView().open();
		},
		error : function() {
			alert("Login incorrecto");
		}
	});
}

$.login.addEventListener('open', function(e) {
	init();
});

Ti.UI.Android.hideSoftKeyboard();
$.user.blur();




