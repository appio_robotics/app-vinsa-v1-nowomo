var args = arguments[0] || {};
var modelAttributes = args.modelAttributes;

var thisWindow = $.closeIncident;

var incidents = Alloy.Collections.incident;


// Tiempo de desplazamiento
function updateTimeSlider(e){
	var value = Math.floor(e.value);
    $.timeLabel.text = "Tiempo de desplazamiento: " + value + " minutos";
}

updateTimeSlider({value: $.timeSlider.value});



// Tiempo en cliente
function updateClientTimeSlider(e){
	var value = Math.floor(e.value);
    $.clientTimeLabel.text = "Tiempo en cliente: " + value + " minutos";
}

updateClientTimeSlider({value: $.clientTimeSlider.value});


// Distancia recorrida
function updateDistanceSlider(e){	
	var value = Math.floor(e.value); 
    $.distanceLabel.text = "Distancia recorrida: " + value + " Km.";
}

updateDistanceSlider({value: $.distanceSlider.value});

function saveAndSend(){
		
		var model = incidents.get(modelAttributes.id);
		
		var funciona = $.chkWorks.getView('checkboxElement').value ? 1 : 0;
		var facturable = $.chkBillable.getView('checkboxElement').value ? 1 : 0;
		var garantia = $.chkWarranty.getView('checkboxElement').value ? 1 : 0;
		
		if($.chkPending.getView('checkboxElement').value){
			var pendiente = $.picker.getSelectedRow(0).title;
		}
		else{
			var pendiente = "";
		}
		
		var tiempodesplazamiento = Math.floor($.timeSlider.value);
		var tiempoencliente = Math.floor($.clientTimeSlider.value);
		var distancia = Math.floor($.distanceSlider.value);
		
		var detalles = $.details.value;
		
		model.set("idestado", "cerrada");
		model.set("funciona", funciona );
		model.set("facturable", facturable );
		model.set("garantia", garantia );
		model.set("pendiente", pendiente );
		model.set("tiempodesplazamiento", tiempodesplazamiento );
		model.set("tiempoencliente", tiempoencliente );
		model.set("distancia", distancia );
		model.set("detalles", detalles );
		
		model.save();
		
		thisWindow.close();
		
}


function logout(e) {
	Alloy.createController('login').getView().open();
}

thisWindow.addEventListener('open', function(e) {	
	Alloy.setActionbar(thisWindow, 'Incidencia');
});
