var Dev = require("/dev/Dev");

var args = arguments[0] || {};
var data = args.data;

var incident = Alloy.Models.instance("incident");


switch(data.attributes.idestado) {
	case 'abierta':
		data.attributes.iconUrl = "/images/icons/circleRed.png";
		break;

	case 'curso':
		data.attributes.iconUrl = "/images/icons/circleOrange.png";
		break;

	case 'cerrada':
		data.attributes.iconUrl = "/images/icons/circleGreen.png";
		break;
}

// This is completely necessary to clear the model before setting the values. Without it, if you open the incidentCard, then close it and then 
// open it again, the values are not shown
var clearAttributes = {	
	iconUrl: false	
};
incident.set(clearAttributes);
/////////

incident.set(data.attributes);

function dataTransform(model) {
	Ti.API.info("dataTransform");
}

function llamar() {
	alert("Esto es LLAMAR. No implementado para esta primera versión. ");
}

function arrive() {
	alert("Marcar llegada");
}

function map() {
	alert("Mapa. No implementado para esta primera versión");
}

function options() {
	alert("Opciones. No implementado para esta primera versión");
}

function close() {
	$.dialog.show();
}

function selectCloseOption(e) {
	if (data.attributes.idestado === "cerrada") {
		alert("Esta incidencia ya está cerrada");
	} else {
		openPopup(e);
	}
}

function openPopup(e) {
	switch(e.index.toString()) {
		case "0":
			var incidents = Alloy.Collections.incident;
			var model = incidents.get(data.attributes.id);

			model.set("idestado", "cerrada");
			model.set("type", "telefono");

			model.save();

			thisWindow.close();

			break;

		case "1":

			Alloy.createController('closeIncident', {
				modelAttributes : data.attributes
			}).getView().open();

			break;
	}
}

var thisWindow = $.incidentCard;

function logout(e) {
	Alloy.createController('login').getView().open();
}

thisWindow.addEventListener('open', function(e) {

	Alloy.setActionbar(thisWindow, 'Listado de incidencias');

});

thisWindow.addEventListener("close", function(){
    $.destroy();
});
