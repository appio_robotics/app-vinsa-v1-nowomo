var args = arguments[0] || {};
var thisWindow = $.incidentsList;

Ti.include("lib/moment.js");

var DataModel = require("/DataModel");
var Dev = require("/dev/Dev");
var params = args.params;

// Get the data from the server
var incidents = Alloy.Collections.incident;

// Encase the title attribute in square brackets
function incidentTransform(model) {
	// Need to convert the model to a JSON object
	var transform = model.toJSON();
	switch(transform.idestado) {
		case 'abierta':
			transform.iconUrl = "/images/icons/circleRed.png";
			transform.estado = "Estado: abierta";
			break;

		case 'curso':
			transform.iconUrl = "/images/icons/circleOrange.png";
			transform.estado = "Estado: en curso";
			break;

		case 'cerrada':
			transform.iconUrl = "/images/icons/circleGreen.png";
			transform.estado = "Estado: cerrada";
			break;
	}
	
	transform.fechaapertura = moment(transform.fechaapertura).valueOf();
	transform.fechacierre = moment(transform.fechacierre).valueOf();

	return transform;
}

function filterFunction(collection) {
	var params = Alloy.Globals.searchParams;

	var currentUser = DataModel.currentUser.get();
	var userId = currentUser['id'];

	return collection.filter(function(model) {
	
		if(model.get('id_tecnico') != userId){
			return false;
		}
		
		var show = false;		

		if (params.abierta) {
			show = show || model.get("idestado") === "abierta";
		}
		if (params.curso) {
			show = show || model.get("idestado") === "curso";
		}
		if (params.cerrada) {
			show = show || model.get("idestado") === "cerrada";
		}
		if(params.fechasSelected){		
			
			Dev.inf("a la entrada show es " + show);
			
			
			
			if(model.get("fechaapertura") != "0000-00-00 00:00:00"){
				var fechaaperturaSelected = moment(params.fechaapertura).valueOf();
				var fechaaperturaModel = moment(model.get("fechaapertura")).valueOf();
				
				Dev.inf("fechaaperturaSelected " + fechaaperturaSelected + "  y fechaaperturaModel " + fechaaperturaModel);
				
				show = show && (fechaaperturaModel > fechaaperturaSelected);
				
				
				Dev.inf("a la salida show es " + show);
			}
			
			if(model.get("fechacierre") != "0000-00-00 00:00:00"){
				
				Dev.inf("dentro del if de fechacierre");
				
				var fechacierreSelected = moment(params.fechacierre).valueOf();
				var fechacierreModel = moment(model.get("fechacierre")).valueOf();
				
				show = show && fechacierreModel < fechacierreSelected;
			}
			// If it has
			else{
				
			}
			
		}

		return show;

	});
}

incidents.fetch({
	success : function(models) {
		//alert("en fetch de incidents tengo models"); alert(models);
		//alert("  con len " + models.length);
	},
	error: function(data){
		//alert("hay un error en el success"); alert(data);
	}
});


function onItemClick(e) {
	var modelId = e.rowData.modelId;

	Alloy.createController('incidentCard', {
		data : incidents.get(modelId)
	}).getView().open();

}

function logout(e) {
	Alloy.createController('login').getView().open();
}

thisWindow.addEventListener('open', function(e) {
	
	Alloy.setActionbar(thisWindow, 'Filtro');

});

