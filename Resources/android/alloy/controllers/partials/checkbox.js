function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "partials/checkbox";
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    $.__views.checkbox = Ti.UI.createView({
        layout: "horizontal",
        height: Ti.UI.SIZE,
        id: "checkbox"
    });
    $.__views.checkbox && $.addTopLevelView($.__views.checkbox);
    $.__views.checkboxElement = Ti.UI.createSwitch({
        style: Ti.UI.Android.SWITCH_STYLE_CHECKBOX,
        left: "0",
        width: Ti.UI.SIZE,
        id: "checkboxElement"
    });
    $.__views.checkbox.add($.__views.checkboxElement);
    $.__views.label = Ti.UI.createLabel({
        color: "#333",
        left: "0",
        width: Ti.UI.SIZE,
        id: "label"
    });
    $.__views.checkbox.add($.__views.label);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    $.label.text = args.title;
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;