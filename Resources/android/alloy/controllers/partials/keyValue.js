function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "partials/keyValue";
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    $.__views.keyValue = Ti.UI.createView({
        layout: "horizontal",
        height: Ti.UI.SIZE,
        left: "0",
        id: "keyValue"
    });
    $.__views.keyValue && $.addTopLevelView($.__views.keyValue);
    $.__views.key = Ti.UI.createLabel({
        color: "#333",
        left: "0",
        font: {
            fontWeight: "bold"
        },
        id: "key"
    });
    $.__views.keyValue.add($.__views.key);
    $.__views.value = Ti.UI.createLabel({
        color: "#333",
        left: "10",
        id: "value"
    });
    $.__views.keyValue.add($.__views.value);
    $.__views.value = Ti.UI.createLabel({
        color: "#333",
        left: "10",
        bindId: "test",
        id: "value"
    });
    $.__views.keyValue.add($.__views.value);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    $.key.text = args.key;
    $.value.text = args.value;
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;