function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function __alloyId13() {
        $.__views.filter.removeEventListener("open", __alloyId13);
        if ($.__views.filter.activity) $.__views.filter.activity.onCreateOptionsMenu = function(e) {
            var __alloyId12 = {
                id: "button",
                title: "Salir"
            };
            $.__views.button = e.menu.add(_.pick(__alloyId12, Alloy.Android.menuItemCreateArgs));
            $.__views.button.applyProperties(_.omit(__alloyId12, Alloy.Android.menuItemCreateArgs));
            logout ? $.__views.button.addEventListener("click", logout) : __defers["$.__views.button!click!logout"] = true;
        }; else {
            Ti.API.warn("You attempted to attach an Android Menu to a lightweight Window");
            Ti.API.warn("or other UI component which does not have an Android activity.");
            Ti.API.warn("Android Menus can only be opened on TabGroups and heavyweight Windows.");
        }
    }
    function init() {
        $.chkOpen.getView("checkboxElement").value = $.chkInProcess.getView("checkboxElement").value = true;
        $.startDate.getView("dateTextField").blur();
        $.startDate.getView("dateTextField").addEventListener("focus", function() {
            if (!initialized) {
                $.startDate.getView("dateTextField").blur();
                initialized = true;
            }
        });
    }
    function search() {
        var abiertaValue = $.chkOpen.getView("checkboxElement").value || false;
        var cursoValue = $.chkInProcess.getView("checkboxElement").value || false;
        var cerradaValue = $.chkClose.getView("checkboxElement").value || false;
        var fechasSelected = $.chkDate.getView("checkboxElement").value || false;
        var fechaapertura = fechasSelected && $.startDate.getView("dateTextField").value || false;
        var fechacierre = fechasSelected && $.endDate.getView("dateTextField").value || false;
        var params = {
            abierta: abiertaValue,
            curso: cursoValue,
            cerrada: cerradaValue,
            fechasSelected: fechasSelected,
            fechaapertura: fechaapertura,
            fechacierre: fechacierre
        };
        Alloy.Globals.searchParams = params;
        Alloy.createController("incidentsList", {
            params: params
        }).getView().open();
    }
    function logout() {
        Alloy.createController("login").getView().open();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "filter";
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.filter = Ti.UI.createWindow({
        layout: "vertical",
        backgroundColor: "white",
        id: "filter"
    });
    $.__views.filter && $.addTopLevelView($.__views.filter);
    $.__views.filter.addEventListener("open", __alloyId13);
    $.__views.__alloyId14 = Ti.UI.createScrollView({
        id: "__alloyId14"
    });
    $.__views.filter.add($.__views.__alloyId14);
    $.__views.__alloyId15 = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        top: "10",
        width: "80%",
        left: "10%",
        id: "__alloyId15"
    });
    $.__views.__alloyId14.add($.__views.__alloyId15);
    $.__views.stateContainer = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        top: "20",
        id: "stateContainer"
    });
    $.__views.__alloyId15.add($.__views.stateContainer);
    $.__views.chkOpen = Alloy.createController("partials/checkbox", {
        id: "chkOpen",
        title: "Abiertas",
        __parentSymbol: $.__views.stateContainer
    });
    $.__views.chkOpen.setParent($.__views.stateContainer);
    $.__views.chkInProcess = Alloy.createController("partials/checkbox", {
        id: "chkInProcess",
        title: "En curso",
        __parentSymbol: $.__views.stateContainer
    });
    $.__views.chkInProcess.setParent($.__views.stateContainer);
    $.__views.chkClose = Alloy.createController("partials/checkbox", {
        id: "chkClose",
        title: "Cerradas",
        __parentSymbol: $.__views.stateContainer
    });
    $.__views.chkClose.setParent($.__views.stateContainer);
    $.__views.__alloyId16 = Alloy.createController("partials/horizontalSeparator", {
        id: "__alloyId16",
        __parentSymbol: $.__views.__alloyId15
    });
    $.__views.__alloyId16.setParent($.__views.__alloyId15);
    $.__views.dateContainer = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        top: "20",
        id: "dateContainer"
    });
    $.__views.__alloyId15.add($.__views.dateContainer);
    $.__views.chkDate = Alloy.createController("partials/checkbox", {
        id: "chkDate",
        title: "Por fechas",
        __parentSymbol: $.__views.dateContainer
    });
    $.__views.chkDate.setParent($.__views.dateContainer);
    $.__views.startDate = Alloy.createController("partials/datePicker", {
        id: "startDate",
        title: "Desde",
        __parentSymbol: $.__views.dateContainer
    });
    $.__views.startDate.setParent($.__views.dateContainer);
    $.__views.endDate = Alloy.createController("partials/datePicker", {
        id: "endDate",
        title: "Hasta",
        __parentSymbol: $.__views.dateContainer
    });
    $.__views.endDate.setParent($.__views.dateContainer);
    $.__views.__alloyId17 = Alloy.createController("partials/horizontalSeparator", {
        id: "__alloyId17",
        __parentSymbol: $.__views.__alloyId15
    });
    $.__views.__alloyId17.setParent($.__views.__alloyId15);
    $.__views.btnSearch = Ti.UI.createButton({
        top: "30dp",
        width: Ti.UI.FILL,
        right: "0",
        title: "Buscar",
        id: "btnSearch"
    });
    $.__views.__alloyId15.add($.__views.btnSearch);
    search ? $.__views.btnSearch.addEventListener("click", search) : __defers["$.__views.btnSearch!click!search"] = true;
    $.__views.__alloyId18 = Ti.UI.createView({
        layout: "vertical",
        height: "30dp",
        id: "__alloyId18"
    });
    $.__views.__alloyId15.add($.__views.__alloyId18);
    exports.destroy = function() {};
    _.extend($, $.__views);
    arguments[0] || {};
    var thisWindow = $.filter;
    var initialized = false;
    thisWindow.addEventListener("open", function() {
        init();
        Alloy.setActionbar(thisWindow, "Menú");
    });
    __defers["$.__views.button!click!logout"] && $.__views.button.addEventListener("click", logout);
    __defers["$.__views.btnSearch!click!search"] && $.__views.btnSearch.addEventListener("click", search);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;