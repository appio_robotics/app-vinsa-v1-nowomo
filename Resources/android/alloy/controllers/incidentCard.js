function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function __alloyId27() {
        $.__views.incidentCard.removeEventListener("open", __alloyId27);
        if ($.__views.incidentCard.activity) $.__views.incidentCard.activity.onCreateOptionsMenu = function(e) {
            var __alloyId26 = {
                id: "button",
                title: "Salir"
            };
            $.__views.button = e.menu.add(_.pick(__alloyId26, Alloy.Android.menuItemCreateArgs));
            $.__views.button.applyProperties(_.omit(__alloyId26, Alloy.Android.menuItemCreateArgs));
            logout ? $.__views.button.addEventListener("click", logout) : __defers["$.__views.button!click!logout"] = true;
        }; else {
            Ti.API.warn("You attempted to attach an Android Menu to a lightweight Window");
            Ti.API.warn("or other UI component which does not have an Android activity.");
            Ti.API.warn("Android Menus can only be opened on TabGroups and heavyweight Windows.");
        }
    }
    function llamar() {
        alert("Esto es LLAMAR. No implementado para esta primera versión. ");
    }
    function arrive() {
        alert("Marcar llegada");
    }
    function map() {
        alert("Mapa. No implementado para esta primera versión");
    }
    function options() {
        alert("Opciones. No implementado para esta primera versión");
    }
    function close() {
        $.dialog.show();
    }
    function selectCloseOption(e) {
        "cerrada" === data.attributes.idestado ? alert("Esta incidencia ya está cerrada") : openPopup(e);
    }
    function openPopup(e) {
        switch (e.index.toString()) {
          case "0":
            var incidents = Alloy.Collections.incident;
            var model = incidents.get(data.attributes.id);
            model.set("idestado", "cerrada");
            model.set("type", "telefono");
            model.save();
            thisWindow.close();
            break;

          case "1":
            Alloy.createController("closeIncident", {
                modelAttributes: data.attributes
            }).getView().open();
        }
    }
    function logout() {
        Alloy.createController("login").getView().open();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "incidentCard";
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    var __defers = {};
    Alloy.Models.instance("incident");
    $.__views.incidentCard = Ti.UI.createWindow({
        layout: "vertical",
        backgroundColor: "white",
        id: "incidentCard"
    });
    $.__views.incidentCard && $.addTopLevelView($.__views.incidentCard);
    $.__views.incidentCard.addEventListener("open", __alloyId27);
    $.__views.__alloyId28 = Ti.UI.createScrollView({
        id: "__alloyId28"
    });
    $.__views.incidentCard.add($.__views.__alloyId28);
    $.__views.__alloyId29 = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        top: "10",
        width: Ti.UI.FILL,
        left: "0",
        id: "__alloyId29"
    });
    $.__views.__alloyId28.add($.__views.__alloyId29);
    $.__views.__alloyId30 = Ti.UI.createScrollView({
        id: "__alloyId30"
    });
    $.__views.__alloyId29.add($.__views.__alloyId30);
    $.__views.infoContainer = Ti.UI.createView({
        layout: "horizontal",
        height: Ti.UI.SIZE,
        width: Ti.UI.FILL,
        id: "infoContainer"
    });
    $.__views.__alloyId30.add($.__views.infoContainer);
    $.__views.stateIcon = Ti.UI.createImageView({
        left: "5%",
        top: "0",
        width: "10%",
        id: "stateIcon"
    });
    $.__views.infoContainer.add($.__views.stateIcon);
    $.__views.textContainer = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        top: "0",
        left: "5%",
        width: "80%",
        id: "textContainer"
    });
    $.__views.infoContainer.add($.__views.textContainer);
    $.__views.firstKeyValueContainer = Ti.UI.createView({
        layout: "horizontal",
        height: Ti.UI.SIZE,
        left: "0",
        width: "95%",
        top: "0dp",
        id: "firstKeyValueContainer"
    });
    $.__views.textContainer.add($.__views.firstKeyValueContainer);
    $.__views.__alloyId31 = Ti.UI.createLabel({
        color: "#333",
        left: "0",
        width: Ti.UI.FILL,
        font: {
            fontWeight: "bold"
        },
        text: "Cliente: ",
        id: "__alloyId31"
    });
    $.__views.firstKeyValueContainer.add($.__views.__alloyId31);
    $.__views.__alloyId32 = Ti.UI.createLabel({
        color: "#333",
        width: Ti.UI.FILL,
        id: "__alloyId32"
    });
    $.__views.firstKeyValueContainer.add($.__views.__alloyId32);
    $.__views.__alloyId33 = Ti.UI.createView({
        layout: "horizontal",
        height: Ti.UI.SIZE,
        left: "0",
        width: "95%",
        top: "10dp",
        id: "__alloyId33"
    });
    $.__views.textContainer.add($.__views.__alloyId33);
    $.__views.__alloyId34 = Ti.UI.createLabel({
        color: "#333",
        left: "0",
        width: Ti.UI.FILL,
        font: {
            fontWeight: "bold"
        },
        text: "Dirección: ",
        id: "__alloyId34"
    });
    $.__views.__alloyId33.add($.__views.__alloyId34);
    $.__views.__alloyId35 = Ti.UI.createLabel({
        color: "#333",
        width: Ti.UI.FILL,
        id: "__alloyId35"
    });
    $.__views.__alloyId33.add($.__views.__alloyId35);
    $.__views.__alloyId36 = Ti.UI.createView({
        layout: "horizontal",
        height: Ti.UI.SIZE,
        left: "0",
        width: "95%",
        top: "10dp",
        id: "__alloyId36"
    });
    $.__views.textContainer.add($.__views.__alloyId36);
    $.__views.__alloyId37 = Ti.UI.createLabel({
        color: "#333",
        left: "0",
        width: Ti.UI.FILL,
        font: {
            fontWeight: "bold"
        },
        text: "Título incidencia: ",
        id: "__alloyId37"
    });
    $.__views.__alloyId36.add($.__views.__alloyId37);
    $.__views.__alloyId38 = Ti.UI.createLabel({
        color: "#333",
        width: Ti.UI.FILL,
        id: "__alloyId38"
    });
    $.__views.__alloyId36.add($.__views.__alloyId38);
    $.__views.__alloyId39 = Ti.UI.createView({
        layout: "horizontal",
        height: Ti.UI.SIZE,
        left: "0",
        width: "95%",
        top: "10dp",
        id: "__alloyId39"
    });
    $.__views.textContainer.add($.__views.__alloyId39);
    $.__views.__alloyId40 = Ti.UI.createLabel({
        color: "#333",
        left: "0",
        width: Ti.UI.FILL,
        font: {
            fontWeight: "bold"
        },
        text: "Teléfono: ",
        id: "__alloyId40"
    });
    $.__views.__alloyId39.add($.__views.__alloyId40);
    $.__views.__alloyId41 = Ti.UI.createLabel({
        color: "#333",
        width: Ti.UI.FILL,
        id: "__alloyId41"
    });
    $.__views.__alloyId39.add($.__views.__alloyId41);
    $.__views.__alloyId42 = Ti.UI.createView({
        layout: "horizontal",
        height: Ti.UI.SIZE,
        left: "0",
        width: "95%",
        top: "10dp",
        id: "__alloyId42"
    });
    $.__views.textContainer.add($.__views.__alloyId42);
    $.__views.__alloyId43 = Ti.UI.createLabel({
        color: "#333",
        left: "0",
        width: Ti.UI.FILL,
        font: {
            fontWeight: "bold"
        },
        text: "Detalle: ",
        id: "__alloyId43"
    });
    $.__views.__alloyId42.add($.__views.__alloyId43);
    $.__views.__alloyId44 = Ti.UI.createLabel({
        color: "#333",
        width: Ti.UI.FILL,
        id: "__alloyId44"
    });
    $.__views.__alloyId42.add($.__views.__alloyId44);
    $.__views.__alloyId45 = Alloy.createController("partials/horizontalSeparator", {
        id: "__alloyId45",
        __parentSymbol: $.__views.__alloyId29
    });
    $.__views.__alloyId45.setParent($.__views.__alloyId29);
    $.__views.buttonsContainer = Ti.UI.createView({
        layout: "horizontal",
        height: Ti.UI.SIZE,
        top: "30",
        id: "buttonsContainer"
    });
    $.__views.__alloyId29.add($.__views.buttonsContainer);
    $.__views.__alloyId46 = Alloy.createController("partials/iconButton", {
        title: "Llamar",
        image: "call.png",
        id: "__alloyId46",
        __parentSymbol: $.__views.buttonsContainer
    });
    $.__views.__alloyId46.setParent($.__views.buttonsContainer);
    llamar ? $.__views.__alloyId46.on("click", llamar) : __defers["$.__views.__alloyId46!click!llamar"] = true;
    $.__views.__alloyId47 = Alloy.createController("partials/iconButton", {
        title: "Marcar llegada",
        image: "arrive.png",
        id: "__alloyId47",
        __parentSymbol: $.__views.buttonsContainer
    });
    $.__views.__alloyId47.setParent($.__views.buttonsContainer);
    arrive ? $.__views.__alloyId47.on("click", arrive) : __defers["$.__views.__alloyId47!click!arrive"] = true;
    $.__views.__alloyId48 = Alloy.createController("partials/iconButton", {
        title: "Mapa",
        image: "map.png",
        id: "__alloyId48",
        __parentSymbol: $.__views.buttonsContainer
    });
    $.__views.__alloyId48.setParent($.__views.buttonsContainer);
    map ? $.__views.__alloyId48.on("click", map) : __defers["$.__views.__alloyId48!click!map"] = true;
    $.__views.__alloyId49 = Alloy.createController("partials/iconButton", {
        title: "Opciones",
        image: "options.png",
        id: "__alloyId49",
        __parentSymbol: $.__views.buttonsContainer
    });
    $.__views.__alloyId49.setParent($.__views.buttonsContainer);
    options ? $.__views.__alloyId49.on("click", options) : __defers["$.__views.__alloyId49!click!options"] = true;
    $.__views.__alloyId50 = Alloy.createController("partials/iconButton", {
        title: "Cerrar incidencia",
        image: "close.png",
        id: "__alloyId50",
        __parentSymbol: $.__views.buttonsContainer
    });
    $.__views.__alloyId50.setParent($.__views.buttonsContainer);
    close ? $.__views.__alloyId50.on("click", close) : __defers["$.__views.__alloyId50!click!close"] = true;
    $.__views.__alloyId51 = Ti.UI.createView({
        layout: "vertical",
        height: "60dp",
        id: "__alloyId51"
    });
    $.__views.buttonsContainer.add($.__views.__alloyId51);
    var __alloyId53 = [];
    __alloyId53.push("Por teléfono");
    __alloyId53.push("Presencial");
    $.__views.dialog = Ti.UI.createOptionDialog({
        options: __alloyId53,
        id: "dialog",
        title: "Marcar llegada"
    });
    selectCloseOption ? $.__views.dialog.addEventListener("click", selectCloseOption) : __defers["$.__views.dialog!click!selectCloseOption"] = true;
    $.__views.__alloyId56 = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        id: "__alloyId56"
    });
    $.__views.__alloyId56 && $.addTopLevelView($.__views.__alloyId56);
    var __alloyId57 = function() {
        $.stateIcon.image = _.isFunction(Alloy.Models.incident.transform) ? Alloy.Models.incident.transform()["iconUrl"] : _.template("<%=incident.iconUrl%>", {
            incident: Alloy.Models.incident.toJSON()
        });
        $.__alloyId32.text = _.isFunction(Alloy.Models.incident.transform) ? Alloy.Models.incident.transform()["clienteNombre"] : _.template("<%=incident.clienteNombre%>", {
            incident: Alloy.Models.incident.toJSON()
        });
        $.__alloyId35.text = _.isFunction(Alloy.Models.incident.transform) ? Alloy.Models.incident.transform()["ctDireccion"] : _.template("<%=incident.ctDireccion%>", {
            incident: Alloy.Models.incident.toJSON()
        });
        $.__alloyId38.text = _.isFunction(Alloy.Models.incident.transform) ? Alloy.Models.incident.transform()["nombre"] : _.template("<%=incident.nombre%>", {
            incident: Alloy.Models.incident.toJSON()
        });
        $.__alloyId41.text = _.isFunction(Alloy.Models.incident.transform) ? Alloy.Models.incident.transform()["ctTelefono"] : _.template("<%=incident.ctTelefono%>", {
            incident: Alloy.Models.incident.toJSON()
        });
        $.__alloyId44.text = _.isFunction(Alloy.Models.incident.transform) ? Alloy.Models.incident.transform()["descripcion_incidencia"] : _.template("<%=incident.descripcion_incidencia%>", {
            incident: Alloy.Models.incident.toJSON()
        });
    };
    Alloy.Models.incident.on("fetch change destroy", __alloyId57);
    exports.destroy = function() {
        Alloy.Models.incident.off("fetch change destroy", __alloyId57);
    };
    _.extend($, $.__views);
    require("/dev/Dev");
    var args = arguments[0] || {};
    var data = args.data;
    var incident = Alloy.Models.instance("incident");
    switch (data.attributes.idestado) {
      case "abierta":
        data.attributes.iconUrl = "/images/icons/circleRed.png";
        break;

      case "curso":
        data.attributes.iconUrl = "/images/icons/circleOrange.png";
        break;

      case "cerrada":
        data.attributes.iconUrl = "/images/icons/circleGreen.png";
    }
    var clearAttributes = {
        iconUrl: false
    };
    incident.set(clearAttributes);
    incident.set(data.attributes);
    var thisWindow = $.incidentCard;
    thisWindow.addEventListener("open", function() {
        Alloy.setActionbar(thisWindow, "Listado de incidencias");
    });
    thisWindow.addEventListener("close", function() {
        $.destroy();
    });
    __defers["$.__views.button!click!logout"] && $.__views.button.addEventListener("click", logout);
    __defers["$.__views.__alloyId46!click!llamar"] && $.__views.__alloyId46.on("click", llamar);
    __defers["$.__views.__alloyId47!click!arrive"] && $.__views.__alloyId47.on("click", arrive);
    __defers["$.__views.__alloyId48!click!map"] && $.__views.__alloyId48.on("click", map);
    __defers["$.__views.__alloyId49!click!options"] && $.__views.__alloyId49.on("click", options);
    __defers["$.__views.__alloyId50!click!close"] && $.__views.__alloyId50.on("click", close);
    __defers["$.__views.dialog!click!selectCloseOption"] && $.__views.dialog.addEventListener("click", selectCloseOption);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;