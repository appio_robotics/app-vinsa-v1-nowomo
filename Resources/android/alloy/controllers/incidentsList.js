function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function __alloyId60() {
        $.__views.incidentsList.removeEventListener("open", __alloyId60);
        if ($.__views.incidentsList.activity) $.__views.incidentsList.activity.onCreateOptionsMenu = function(e) {
            var __alloyId59 = {
                id: "button",
                title: "Salir"
            };
            $.__views.button = e.menu.add(_.pick(__alloyId59, Alloy.Android.menuItemCreateArgs));
            $.__views.button.applyProperties(_.omit(__alloyId59, Alloy.Android.menuItemCreateArgs));
            logout ? $.__views.button.addEventListener("click", logout) : __defers["$.__views.button!click!logout"] = true;
        }; else {
            Ti.API.warn("You attempted to attach an Android Menu to a lightweight Window");
            Ti.API.warn("or other UI component which does not have an Android activity.");
            Ti.API.warn("Android Menus can only be opened on TabGroups and heavyweight Windows.");
        }
    }
    function __alloyId78(e) {
        if (e && e.fromAdapter) return;
        __alloyId78.opts || {};
        var models = filterFunction(__alloyId77);
        var len = models.length;
        var rows = [];
        for (var i = 0; len > i; i++) {
            var __alloyId62 = models[i];
            __alloyId62.__transform = incidentTransform(__alloyId62);
            var __alloyId64 = Ti.UI.createTableViewRow({
                layout: "horizontal",
                width: Ti.UI.FILL,
                modelId: "undefined" != typeof __alloyId62.__transform["id"] ? __alloyId62.__transform["id"] : __alloyId62.get("id")
            });
            rows.push(__alloyId64);
            var __alloyId66 = Ti.UI.createView({
                layout: "vertical",
                height: Ti.UI.SIZE,
                width: "20%"
            });
            __alloyId64.add(__alloyId66);
            var __alloyId68 = Ti.UI.createImageView({
                width: "40%",
                image: "undefined" != typeof __alloyId62.__transform["iconUrl"] ? __alloyId62.__transform["iconUrl"] : __alloyId62.get("iconUrl")
            });
            __alloyId66.add(__alloyId68);
            var __alloyId70 = Ti.UI.createView({
                layout: "vertical",
                height: Ti.UI.SIZE,
                width: "80%"
            });
            __alloyId64.add(__alloyId70);
            var __alloyId72 = Ti.UI.createLabel({
                color: "#333",
                top: "10",
                left: "0",
                width: Ti.UI.SIZE,
                height: Ti.UI.SIZE,
                font: {
                    fontSize: "18dp",
                    fontWeight: "bold"
                },
                text: "undefined" != typeof __alloyId62.__transform["clienteNombre"] ? __alloyId62.__transform["clienteNombre"] : __alloyId62.get("clienteNombre")
            });
            __alloyId70.add(__alloyId72);
            var __alloyId74 = Ti.UI.createLabel({
                color: "#333",
                top: "10",
                left: "0",
                width: Ti.UI.SIZE,
                height: Ti.UI.SIZE,
                font: {
                    fontSize: "14dp"
                },
                text: "undefined" != typeof __alloyId62.__transform["nombre"] ? __alloyId62.__transform["nombre"] : __alloyId62.get("nombre")
            });
            __alloyId70.add(__alloyId74);
            var __alloyId76 = Ti.UI.createLabel({
                color: "#333",
                top: "10",
                left: "0",
                width: Ti.UI.SIZE,
                height: Ti.UI.SIZE,
                font: {
                    fontSize: "14dp"
                },
                text: "undefined" != typeof __alloyId62.__transform["estado"] ? __alloyId62.__transform["estado"] : __alloyId62.get("estado")
            });
            __alloyId70.add(__alloyId76);
        }
        $.__views.incidentTable.setData(rows);
    }
    function incidentTransform(model) {
        var transform = model.toJSON();
        switch (transform.idestado) {
          case "abierta":
            transform.iconUrl = "/images/icons/circleRed.png";
            transform.estado = "Estado: abierta";
            break;

          case "curso":
            transform.iconUrl = "/images/icons/circleOrange.png";
            transform.estado = "Estado: en curso";
            break;

          case "cerrada":
            transform.iconUrl = "/images/icons/circleGreen.png";
            transform.estado = "Estado: cerrada";
        }
        transform.fechaapertura = moment(transform.fechaapertura).valueOf();
        transform.fechacierre = moment(transform.fechacierre).valueOf();
        return transform;
    }
    function filterFunction(collection) {
        var params = Alloy.Globals.searchParams;
        var currentUser = DataModel.currentUser.get();
        var userId = currentUser["id"];
        return collection.filter(function(model) {
            if (model.get("id_tecnico") != userId) return false;
            var show = false;
            params.abierta && (show = show || "abierta" === model.get("idestado"));
            params.curso && (show = show || "curso" === model.get("idestado"));
            params.cerrada && (show = show || "cerrada" === model.get("idestado"));
            if (params.fechasSelected) {
                Dev.inf("a la entrada show es " + show);
                if ("0000-00-00 00:00:00" != model.get("fechaapertura")) {
                    var fechaaperturaSelected = moment(params.fechaapertura).valueOf();
                    var fechaaperturaModel = moment(model.get("fechaapertura")).valueOf();
                    Dev.inf("fechaaperturaSelected " + fechaaperturaSelected + "  y fechaaperturaModel " + fechaaperturaModel);
                    show = show && fechaaperturaModel > fechaaperturaSelected;
                    Dev.inf("a la salida show es " + show);
                }
                if ("0000-00-00 00:00:00" != model.get("fechacierre")) {
                    Dev.inf("dentro del if de fechacierre");
                    var fechacierreSelected = moment(params.fechacierre).valueOf();
                    var fechacierreModel = moment(model.get("fechacierre")).valueOf();
                    show = show && fechacierreSelected > fechacierreModel;
                }
            }
            return show;
        });
    }
    function onItemClick(e) {
        var modelId = e.rowData.modelId;
        Alloy.createController("incidentCard", {
            data: incidents.get(modelId)
        }).getView().open();
    }
    function logout() {
        Alloy.createController("login").getView().open();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "incidentsList";
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.incidentsList = Ti.UI.createWindow({
        layout: "vertical",
        backgroundColor: "white",
        id: "incidentsList"
    });
    $.__views.incidentsList && $.addTopLevelView($.__views.incidentsList);
    $.__views.incidentsList.addEventListener("open", __alloyId60);
    $.__views.__alloyId61 = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.FILL,
        top: "0",
        width: Ti.UI.FILL,
        left: "0",
        id: "__alloyId61"
    });
    $.__views.incidentsList.add($.__views.__alloyId61);
    $.__views.incidentTable = Ti.UI.createTableView({
        height: Ti.UI.FILL,
        scrollable: true,
        id: "incidentTable"
    });
    $.__views.__alloyId61.add($.__views.incidentTable);
    var __alloyId77 = Alloy.Collections["incident"] || incident;
    __alloyId77.on("fetch destroy change add remove reset", __alloyId78);
    onItemClick ? $.__views.incidentTable.addEventListener("click", onItemClick) : __defers["$.__views.incidentTable!click!onItemClick"] = true;
    exports.destroy = function() {
        __alloyId77.off("fetch destroy change add remove reset", __alloyId78);
    };
    _.extend($, $.__views);
    var args = arguments[0] || {};
    var thisWindow = $.incidentsList;
    Ti.include("lib/moment.js");
    var DataModel = require("/DataModel");
    var Dev = require("/dev/Dev");
    args.params;
    var incidents = Alloy.Collections.incident;
    incidents.fetch({
        success: function() {},
        error: function() {}
    });
    thisWindow.addEventListener("open", function() {
        Alloy.setActionbar(thisWindow, "Filtro");
    });
    __defers["$.__views.button!click!logout"] && $.__views.button.addEventListener("click", logout);
    __defers["$.__views.incidentTable!click!onItemClick"] && $.__views.incidentTable.addEventListener("click", onItemClick);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;