function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function __alloyId82() {
        $.__views.main.removeEventListener("open", __alloyId82);
        if ($.__views.main.activity) $.__views.main.activity.onCreateOptionsMenu = function() {}; else {
            Ti.API.warn("You attempted to attach an Android Menu to a lightweight Window");
            Ti.API.warn("or other UI component which does not have an Android activity.");
            Ti.API.warn("Android Menus can only be opened on TabGroups and heavyweight Windows.");
        }
    }
    function showClients() {
        alert("Aún no está disponible");
    }
    function showIncidents() {
        Alloy.createController("filter").getView().open();
    }
    function showPrint() {
        alert("Aún no está disponible");
    }
    function showNotes() {
        alert("Aún no está disponible");
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "main";
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.main = Ti.UI.createWindow({
        layout: "vertical",
        backgroundColor: "white",
        title: "Menú",
        id: "main"
    });
    $.__views.main && $.addTopLevelView($.__views.main);
    $.__views.main.addEventListener("open", __alloyId82);
    $.__views.mainContainer = Ti.UI.createView({
        layout: "vertical",
        height: "70%",
        top: "15%",
        width: Ti.UI.FILL,
        id: "mainContainer"
    });
    $.__views.main.add($.__views.mainContainer);
    $.__views.rowUp = Ti.UI.createView({
        layout: "horizontal",
        height: "50%",
        id: "rowUp"
    });
    $.__views.mainContainer.add($.__views.rowUp);
    $.__views.iconClients = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        width: "50%",
        id: "iconClients"
    });
    $.__views.rowUp.add($.__views.iconClients);
    showClients ? $.__views.iconClients.addEventListener("click", showClients) : __defers["$.__views.iconClients!click!showClients"] = true;
    $.__views.__alloyId83 = Ti.UI.createImageView({
        left: "20%",
        top: "20%",
        width: "60%",
        height: "60%",
        image: "/images/menuIcons/clients.png",
        id: "__alloyId83"
    });
    $.__views.iconClients.add($.__views.__alloyId83);
    $.__views.__alloyId84 = Ti.UI.createLabel({
        color: "#333",
        text: "Clientes",
        id: "__alloyId84"
    });
    $.__views.iconClients.add($.__views.__alloyId84);
    $.__views.iconIncidents = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        width: "50%",
        id: "iconIncidents"
    });
    $.__views.rowUp.add($.__views.iconIncidents);
    showIncidents ? $.__views.iconIncidents.addEventListener("click", showIncidents) : __defers["$.__views.iconIncidents!click!showIncidents"] = true;
    $.__views.__alloyId85 = Ti.UI.createImageView({
        left: "20%",
        top: "20%",
        width: "60%",
        height: "60%",
        image: "/images/menuIcons/incidents.png",
        id: "__alloyId85"
    });
    $.__views.iconIncidents.add($.__views.__alloyId85);
    $.__views.__alloyId86 = Ti.UI.createLabel({
        color: "#333",
        text: "Incidencias",
        id: "__alloyId86"
    });
    $.__views.iconIncidents.add($.__views.__alloyId86);
    $.__views.rowDown = Ti.UI.createView({
        layout: "horizontal",
        height: "50%",
        id: "rowDown"
    });
    $.__views.mainContainer.add($.__views.rowDown);
    $.__views.iconNotes = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        width: "50%",
        id: "iconNotes"
    });
    $.__views.rowDown.add($.__views.iconNotes);
    showNotes ? $.__views.iconNotes.addEventListener("click", showNotes) : __defers["$.__views.iconNotes!click!showNotes"] = true;
    $.__views.__alloyId87 = Ti.UI.createImageView({
        left: "20%",
        top: "20%",
        width: "60%",
        height: "60%",
        image: "/images/menuIcons/notes.png",
        id: "__alloyId87"
    });
    $.__views.iconNotes.add($.__views.__alloyId87);
    $.__views.__alloyId88 = Ti.UI.createLabel({
        color: "#333",
        text: "Apuntes",
        id: "__alloyId88"
    });
    $.__views.iconNotes.add($.__views.__alloyId88);
    $.__views.iconPrint = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        width: "50%",
        id: "iconPrint"
    });
    $.__views.rowDown.add($.__views.iconPrint);
    showPrint ? $.__views.iconPrint.addEventListener("click", showPrint) : __defers["$.__views.iconPrint!click!showPrint"] = true;
    $.__views.__alloyId89 = Ti.UI.createImageView({
        left: "20%",
        top: "20%",
        width: "60%",
        height: "60%",
        image: "/images/menuIcons/print.png",
        id: "__alloyId89"
    });
    $.__views.iconPrint.add($.__views.__alloyId89);
    $.__views.__alloyId90 = Ti.UI.createLabel({
        color: "#333",
        text: "Imprimir",
        id: "__alloyId90"
    });
    $.__views.iconPrint.add($.__views.__alloyId90);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var thisWindow = $.main;
    thisWindow.addEventListener("open", function() {
        Alloy.setActionbar(thisWindow, "Vinsa App", notLogo = true);
    });
    __defers["$.__views.iconClients!click!showClients"] && $.__views.iconClients.addEventListener("click", showClients);
    __defers["$.__views.iconIncidents!click!showIncidents"] && $.__views.iconIncidents.addEventListener("click", showIncidents);
    __defers["$.__views.iconNotes!click!showNotes"] && $.__views.iconNotes.addEventListener("click", showNotes);
    __defers["$.__views.iconPrint!click!showPrint"] && $.__views.iconPrint.addEventListener("click", showPrint);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;