function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function __alloyId2() {
        $.__views.closeIncident.removeEventListener("open", __alloyId2);
        if ($.__views.closeIncident.activity) $.__views.closeIncident.activity.onCreateOptionsMenu = function(e) {
            var __alloyId1 = {
                id: "button",
                title: "Salir"
            };
            $.__views.button = e.menu.add(_.pick(__alloyId1, Alloy.Android.menuItemCreateArgs));
            $.__views.button.applyProperties(_.omit(__alloyId1, Alloy.Android.menuItemCreateArgs));
            logout ? $.__views.button.addEventListener("click", logout) : __defers["$.__views.button!click!logout"] = true;
        }; else {
            Ti.API.warn("You attempted to attach an Android Menu to a lightweight Window");
            Ti.API.warn("or other UI component which does not have an Android activity.");
            Ti.API.warn("Android Menus can only be opened on TabGroups and heavyweight Windows.");
        }
    }
    function updateTimeSlider(e) {
        var value = Math.floor(e.value);
        $.timeLabel.text = "Tiempo de desplazamiento: " + value + " minutos";
    }
    function updateClientTimeSlider(e) {
        var value = Math.floor(e.value);
        $.clientTimeLabel.text = "Tiempo en cliente: " + value + " minutos";
    }
    function updateDistanceSlider(e) {
        var value = Math.floor(e.value);
        $.distanceLabel.text = "Distancia recorrida: " + value + " Km.";
    }
    function saveAndSend() {
        var model = incidents.get(modelAttributes.id);
        var funciona = $.chkWorks.getView("checkboxElement").value ? 1 : 0;
        var facturable = $.chkBillable.getView("checkboxElement").value ? 1 : 0;
        var garantia = $.chkWarranty.getView("checkboxElement").value ? 1 : 0;
        if ($.chkPending.getView("checkboxElement").value) var pendiente = $.picker.getSelectedRow(0).title; else var pendiente = "";
        var tiempodesplazamiento = Math.floor($.timeSlider.value);
        var tiempoencliente = Math.floor($.clientTimeSlider.value);
        var distancia = Math.floor($.distanceSlider.value);
        var detalles = $.details.value;
        model.set("idestado", "cerrada");
        model.set("funciona", funciona);
        model.set("facturable", facturable);
        model.set("garantia", garantia);
        model.set("pendiente", pendiente);
        model.set("tiempodesplazamiento", tiempodesplazamiento);
        model.set("tiempoencliente", tiempoencliente);
        model.set("distancia", distancia);
        model.set("detalles", detalles);
        model.save();
        thisWindow.close();
    }
    function logout() {
        Alloy.createController("login").getView().open();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "closeIncident";
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.closeIncident = Ti.UI.createWindow({
        layout: "vertical",
        backgroundColor: "white",
        id: "closeIncident"
    });
    $.__views.closeIncident && $.addTopLevelView($.__views.closeIncident);
    $.__views.closeIncident.addEventListener("open", __alloyId2);
    $.__views.__alloyId3 = Ti.UI.createScrollView({
        id: "__alloyId3"
    });
    $.__views.closeIncident.add($.__views.__alloyId3);
    $.__views.__alloyId4 = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        top: "10",
        width: "80%",
        left: "10%",
        id: "__alloyId4"
    });
    $.__views.__alloyId3.add($.__views.__alloyId4);
    $.__views.chkWorks = Alloy.createController("partials/checkbox", {
        id: "chkWorks",
        title: "Funciona",
        __parentSymbol: $.__views.__alloyId4
    });
    $.__views.chkWorks.setParent($.__views.__alloyId4);
    $.__views.chkBillable = Alloy.createController("partials/checkbox", {
        id: "chkBillable",
        title: "Facturable",
        __parentSymbol: $.__views.__alloyId4
    });
    $.__views.chkBillable.setParent($.__views.__alloyId4);
    $.__views.chkWarranty = Alloy.createController("partials/checkbox", {
        id: "chkWarranty",
        title: "Garantía",
        __parentSymbol: $.__views.__alloyId4
    });
    $.__views.chkWarranty.setParent($.__views.__alloyId4);
    $.__views.__alloyId5 = Alloy.createController("partials/horizontalSeparator", {
        id: "__alloyId5",
        __parentSymbol: $.__views.__alloyId4
    });
    $.__views.__alloyId5.setParent($.__views.__alloyId4);
    $.__views.chkPending = Alloy.createController("partials/checkbox", {
        id: "chkPending",
        title: "Pendiente",
        __parentSymbol: $.__views.__alloyId4
    });
    $.__views.chkPending.setParent($.__views.__alloyId4);
    $.__views.picker = Ti.UI.createPicker({
        width: Ti.UI.FILL,
        id: "picker",
        selectionIndicator: "true"
    });
    $.__views.__alloyId4.add($.__views.picker);
    var __alloyId6 = [];
    $.__views.column1 = Ti.UI.createPickerColumn({
        id: "column1"
    });
    __alloyId6.push($.__views.column1);
    $.__views.__alloyId7 = Ti.UI.createPickerRow({
        title: "Reparación de equipos",
        id: "__alloyId7"
    });
    $.__views.column1.addRow($.__views.__alloyId7);
    $.__views.__alloyId8 = Ti.UI.createPickerRow({
        title: "Otros",
        id: "__alloyId8"
    });
    $.__views.column1.addRow($.__views.__alloyId8);
    $.__views.picker.add(__alloyId6);
    $.__views.__alloyId9 = Alloy.createController("partials/horizontalSeparator", {
        id: "__alloyId9",
        __parentSymbol: $.__views.__alloyId4
    });
    $.__views.__alloyId9.setParent($.__views.__alloyId4);
    $.__views.distanceLabel = Ti.UI.createLabel({
        color: "#333",
        top: "30dp",
        left: "0dp",
        text: "Distancia recorrida",
        id: "distanceLabel"
    });
    $.__views.__alloyId4.add($.__views.distanceLabel);
    $.__views.distanceSlider = Ti.UI.createSlider({
        width: Ti.UI.FILL,
        id: "distanceSlider",
        min: "0",
        max: "300",
        value: "0"
    });
    $.__views.__alloyId4.add($.__views.distanceSlider);
    updateDistanceSlider ? $.__views.distanceSlider.addEventListener("change", updateDistanceSlider) : __defers["$.__views.distanceSlider!change!updateDistanceSlider"] = true;
    $.__views.timeLabel = Ti.UI.createLabel({
        color: "#333",
        top: "30dp",
        left: "0dp",
        text: "Tiempo de desplazamiento",
        id: "timeLabel"
    });
    $.__views.__alloyId4.add($.__views.timeLabel);
    $.__views.timeSlider = Ti.UI.createSlider({
        width: Ti.UI.FILL,
        id: "timeSlider",
        min: "0",
        max: "480",
        value: "0"
    });
    $.__views.__alloyId4.add($.__views.timeSlider);
    updateTimeSlider ? $.__views.timeSlider.addEventListener("change", updateTimeSlider) : __defers["$.__views.timeSlider!change!updateTimeSlider"] = true;
    $.__views.clientTimeLabel = Ti.UI.createLabel({
        color: "#333",
        top: "30dp",
        left: "0dp",
        text: "Tiempo en el cliente",
        id: "clientTimeLabel"
    });
    $.__views.__alloyId4.add($.__views.clientTimeLabel);
    $.__views.clientTimeSlider = Ti.UI.createSlider({
        width: Ti.UI.FILL,
        id: "clientTimeSlider",
        min: "0",
        max: "480",
        value: "0"
    });
    $.__views.__alloyId4.add($.__views.clientTimeSlider);
    updateClientTimeSlider ? $.__views.clientTimeSlider.addEventListener("change", updateClientTimeSlider) : __defers["$.__views.clientTimeSlider!change!updateClientTimeSlider"] = true;
    $.__views.details = Ti.UI.createTextArea({
        width: Ti.UI.FILL,
        top: "30dp",
        id: "details",
        hintText: "Detalles"
    });
    $.__views.__alloyId4.add($.__views.details);
    $.__views.__alloyId10 = Alloy.createController("partials/horizontalSeparator", {
        id: "__alloyId10",
        __parentSymbol: $.__views.__alloyId4
    });
    $.__views.__alloyId10.setParent($.__views.__alloyId4);
    $.__views.btnSearch = Ti.UI.createButton({
        top: "30dp",
        width: Ti.UI.FILL,
        title: "Guardar y enviar",
        id: "btnSearch"
    });
    $.__views.__alloyId4.add($.__views.btnSearch);
    saveAndSend ? $.__views.btnSearch.addEventListener("click", saveAndSend) : __defers["$.__views.btnSearch!click!saveAndSend"] = true;
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    var modelAttributes = args.modelAttributes;
    var thisWindow = $.closeIncident;
    var incidents = Alloy.Collections.incident;
    updateTimeSlider({
        value: $.timeSlider.value
    });
    updateClientTimeSlider({
        value: $.clientTimeSlider.value
    });
    updateDistanceSlider({
        value: $.distanceSlider.value
    });
    thisWindow.addEventListener("open", function() {
        Alloy.setActionbar(thisWindow, "Incidencia");
    });
    __defers["$.__views.button!click!logout"] && $.__views.button.addEventListener("click", logout);
    __defers["$.__views.distanceSlider!change!updateDistanceSlider"] && $.__views.distanceSlider.addEventListener("change", updateDistanceSlider);
    __defers["$.__views.timeSlider!change!updateTimeSlider"] && $.__views.timeSlider.addEventListener("change", updateTimeSlider);
    __defers["$.__views.clientTimeSlider!change!updateClientTimeSlider"] && $.__views.clientTimeSlider.addEventListener("change", updateClientTimeSlider);
    __defers["$.__views.btnSearch!click!saveAndSend"] && $.__views.btnSearch.addEventListener("click", saveAndSend);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;