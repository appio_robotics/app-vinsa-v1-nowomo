var Alloy = require("alloy"), _ = require("alloy/underscore")._, model, collection;

var env = "pre";

var url = "http://gestionvinsa.es/" + env + "/public/rest";

exports.definition = {
    config: {
        debug: 1,
        columns: {
            id: "INTEGER",
            nombre: "TEXT",
            apellido: "TEXT",
            role: "TEXT",
            userName: "TEXT",
            password: "TEXT",
            salt: "TEXT"
        },
        URL: url,
        adapter: {
            type: "sqlrest",
            collection_name: "user",
            idAttribute: "id"
        },
        deleteAllOnFetch: true,
        parentNode: "users"
    },
    extendModel: function(Model) {
        _.extend(Model.prototype, {
            toJson: function() {
                var json = this.toJSON();
                return _.extend(json, {
                    iconUrl: "/images/icons/circleGreen.png"
                });
            }
        });
        return Model;
    },
    extendCollection: function(Collection) {
        _.extend(Collection.prototype, {});
        return Collection;
    }
};

model = Alloy.M("user", exports.definition, []);

collection = Alloy.C("user", exports.definition, model);

exports.Model = model;

exports.Collection = collection;