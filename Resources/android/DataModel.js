var Dev = require("/dev/Dev");

exports.currentUser = {
    get: function() {
        var currentUser = JSON.parse(Ti.App.Properties.getString("currentUser"));
        return currentUser;
    },
    set: function(currentUser) {
        Ti.App.Properties.setString("currentUser", JSON.stringify(currentUser));
    }
};