function aux_mixin(target, source) {
    var name, s;
    for (name in source) {
        s = source[name];
        name in target && (target[name] === s || name in empty && empty[name] === s) || (target[name] = s);
    }
    return target;
}

function mixin(obj) {
    obj || (obj = {});
    for (var i = 1, l = arguments.length; l > i; i++) aux_mixin(obj, arguments[i]);
    return obj;
}

function combine(obj1, obj2) {
    for (var p in obj2) obj2.hasOwnProperty(p) && (obj1[p] = "object" == typeof obj2[p] ? combine(obj1[p], obj2[p]) : obj2[p]);
    return obj1;
}

var empty = {};

exports.mixin = mixin;

exports.combine = combine;

exports.isAndroid = function() {
    return "android" === Ti.Platform.osname ? true : false;
};

exports.isSimulator = function() {
    return "Simulator" === Ti.Platform.model || -1 !== Ti.Platform.model.indexOf("sdk") ? true : false;
};

exports.trim = function(str) {
    return str.replace(/^\s+|\s+$/g, "");
};