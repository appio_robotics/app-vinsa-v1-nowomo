var Dev = require("/dev/Dev");

var Utils = require("/Utils");

var DataModel = require("/DataModel");

exports.getUserDataIfRememberMe = function() {
    var currentUser = DataModel.currentUser.get();
    if ("undefined" == typeof currentUser || !currentUser) return false;
    var userData;
    userData = currentUser["rememberMe"] ? currentUser : false;
    return userData;
};

exports.isLogged = function() {
    var isLogged;
    var currentUser = DataModel.currentUser.get();
    isLogged = currentUser && currentUser.isLogged ? true : false;
    return isLogged;
};

exports.checkLogin = function(_args) {
    var userNameReceived = _args.user, passwordReceived = _args.psw, rememberMe = _args.rememberMe;
    var user = Alloy.Collections.user;
    user.fetch({
        sql: {
            where: {
                userName: userNameReceived
            }
        },
        success: function(models) {
            Dev.inf("recibo del server model " + JSON.stringify(models));
            var loginSuccess = false;
            if (models.length > 0) {
                var theUserModel = models.pop();
                var theUser = theUserModel.toJson();
                var md5Password = Titanium.Utils.md5HexDigest(passwordReceived + theUser["salt"]);
                loginSuccess = md5Password === theUser["password"];
            }
            if (loginSuccess) {
                exports.login({
                    currentUser: Utils.combine(theUser, {
                        passwordInForm: passwordReceived
                    }),
                    rememberMe: rememberMe
                });
                _args.success && _args.success();
            } else _args.error && _args.error();
        },
        error: function(data) {
            alert("hay un error en el success");
            alert(data);
        }
    });
};

exports.login = function(_args) {
    var data = Utils.combine(_args.currentUser, {
        rememberMe: _args.rememberMe,
        isLogged: true
    });
    DataModel.currentUser.set(data);
    require("/PushManager").subscribe();
};

exports.logout = function() {
    var currentUser = DataModel.currentUser.get();
    var data;
    if (currentUser["rememberMe"]) {
        data = currentUser;
        data.isLogged = false;
    } else data = false;
    DataModel.currentUser.set(data);
    Alloy.createController("login").getView().open();
};