var Alloy = require("alloy"), _ = require("alloy/underscore")._, model, collection;

var env = "pre";

var url = "http://gestionvinsa.es/" + env + "/public/rest";

exports.definition = {
    config: {
        debug: 1,
        columns: {
            id: "INTEGER",
            nombre: "TEXT",
            idestado: "TEXT",
            id_tecnico: "TEXT",
            clienteNombre: "TEXT",
            ctDireccion: "TEXT",
            ctTelefono: "TEXT",
            fechaapertura: "TEXT",
            fechacierre: "TEXT",
            descripcion_incidencia: "TEXT"
        },
        URL: url,
        adapter: {
            type: "sqlrest",
            collection_name: "incident",
            idAttribute: "id"
        },
        deleteAllOnFetch: true,
        parentNode: "incidents"
    },
    extendModel: function(Model) {
        _.extend(Model.prototype, {
            toJson: function() {
                var json = this.toJSON();
                return _.extend(json, {
                    iconUrl: "/images/icons/circleGreen.png"
                });
            }
        });
        return Model;
    },
    extendCollection: function(Collection) {
        _.extend(Collection.prototype, {});
        return Collection;
    }
};

model = Alloy.M("incident", exports.definition, []);

collection = Alloy.C("incident", exports.definition, model);

exports.Model = model;

exports.Collection = collection;