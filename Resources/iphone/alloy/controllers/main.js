function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function showClients() {
        alert("Aún no está disponible");
    }
    function showIncidents() {
        Alloy.createController("filter").getView().open();
    }
    function showPrint() {
        alert("Aún no está disponible");
    }
    function showNotes() {
        alert("Aún no está disponible");
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "main";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.main = Ti.UI.createWindow({
        layout: "vertical",
        backgroundColor: "white",
        title: "Menú",
        id: "main"
    });
    $.__views.main && $.addTopLevelView($.__views.main);
    $.__views.mainContainer = Ti.UI.createView({
        layout: "vertical",
        height: "70%",
        top: "15%",
        width: Ti.UI.FILL,
        id: "mainContainer"
    });
    $.__views.main.add($.__views.mainContainer);
    $.__views.rowUp = Ti.UI.createView({
        layout: "horizontal",
        height: "50%",
        id: "rowUp"
    });
    $.__views.mainContainer.add($.__views.rowUp);
    $.__views.iconClients = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        width: "50%",
        id: "iconClients"
    });
    $.__views.rowUp.add($.__views.iconClients);
    showClients ? $.__views.iconClients.addEventListener("click", showClients) : __defers["$.__views.iconClients!click!showClients"] = true;
    $.__views.__alloyId69 = Ti.UI.createImageView({
        left: "20%",
        top: "20%",
        width: "60%",
        height: "60%",
        image: "/images/menuIcons/clients.png",
        id: "__alloyId69"
    });
    $.__views.iconClients.add($.__views.__alloyId69);
    $.__views.__alloyId70 = Ti.UI.createLabel({
        color: "#333",
        text: "Clientes",
        id: "__alloyId70"
    });
    $.__views.iconClients.add($.__views.__alloyId70);
    $.__views.iconIncidents = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        width: "50%",
        id: "iconIncidents"
    });
    $.__views.rowUp.add($.__views.iconIncidents);
    showIncidents ? $.__views.iconIncidents.addEventListener("click", showIncidents) : __defers["$.__views.iconIncidents!click!showIncidents"] = true;
    $.__views.__alloyId71 = Ti.UI.createImageView({
        left: "20%",
        top: "20%",
        width: "60%",
        height: "60%",
        image: "/images/menuIcons/incidents.png",
        id: "__alloyId71"
    });
    $.__views.iconIncidents.add($.__views.__alloyId71);
    $.__views.__alloyId72 = Ti.UI.createLabel({
        color: "#333",
        text: "Incidencias",
        id: "__alloyId72"
    });
    $.__views.iconIncidents.add($.__views.__alloyId72);
    $.__views.rowDown = Ti.UI.createView({
        layout: "horizontal",
        height: "50%",
        id: "rowDown"
    });
    $.__views.mainContainer.add($.__views.rowDown);
    $.__views.iconNotes = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        width: "50%",
        id: "iconNotes"
    });
    $.__views.rowDown.add($.__views.iconNotes);
    showNotes ? $.__views.iconNotes.addEventListener("click", showNotes) : __defers["$.__views.iconNotes!click!showNotes"] = true;
    $.__views.__alloyId73 = Ti.UI.createImageView({
        left: "20%",
        top: "20%",
        width: "60%",
        height: "60%",
        image: "/images/menuIcons/notes.png",
        id: "__alloyId73"
    });
    $.__views.iconNotes.add($.__views.__alloyId73);
    $.__views.__alloyId74 = Ti.UI.createLabel({
        color: "#333",
        text: "Apuntes",
        id: "__alloyId74"
    });
    $.__views.iconNotes.add($.__views.__alloyId74);
    $.__views.iconPrint = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        width: "50%",
        id: "iconPrint"
    });
    $.__views.rowDown.add($.__views.iconPrint);
    showPrint ? $.__views.iconPrint.addEventListener("click", showPrint) : __defers["$.__views.iconPrint!click!showPrint"] = true;
    $.__views.__alloyId75 = Ti.UI.createImageView({
        left: "20%",
        top: "20%",
        width: "60%",
        height: "60%",
        image: "/images/menuIcons/print.png",
        id: "__alloyId75"
    });
    $.__views.iconPrint.add($.__views.__alloyId75);
    $.__views.__alloyId76 = Ti.UI.createLabel({
        color: "#333",
        text: "Imprimir",
        id: "__alloyId76"
    });
    $.__views.iconPrint.add($.__views.__alloyId76);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var thisWindow = $.main;
    thisWindow.addEventListener("open", function() {
        Alloy.setActionbar(thisWindow, "Vinsa App", notLogo = true);
    });
    __defers["$.__views.iconClients!click!showClients"] && $.__views.iconClients.addEventListener("click", showClients);
    __defers["$.__views.iconIncidents!click!showIncidents"] && $.__views.iconIncidents.addEventListener("click", showIncidents);
    __defers["$.__views.iconNotes!click!showNotes"] && $.__views.iconNotes.addEventListener("click", showNotes);
    __defers["$.__views.iconPrint!click!showPrint"] && $.__views.iconPrint.addEventListener("click", showPrint);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;