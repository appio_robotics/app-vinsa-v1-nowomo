function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function init() {
        $.chkOpen.getView("checkboxElement").value = $.chkInProcess.getView("checkboxElement").value = true;
        $.startDate.getView("dateTextField").blur();
        $.startDate.getView("dateTextField").addEventListener("focus", function() {
            if (!initialized) {
                $.startDate.getView("dateTextField").blur();
                initialized = true;
            }
        });
    }
    function search() {
        var abiertaValue = $.chkOpen.getView("checkboxElement").value || false;
        var cursoValue = $.chkInProcess.getView("checkboxElement").value || false;
        var cerradaValue = $.chkClose.getView("checkboxElement").value || false;
        var fechasSelected = $.chkDate.getView("checkboxElement").value || false;
        var fechaapertura = fechasSelected && $.startDate.getView("dateTextField").value || false;
        var fechacierre = fechasSelected && $.endDate.getView("dateTextField").value || false;
        var params = {
            abierta: abiertaValue,
            curso: cursoValue,
            cerrada: cerradaValue,
            fechasSelected: fechasSelected,
            fechaapertura: fechaapertura,
            fechacierre: fechacierre
        };
        Alloy.Globals.searchParams = params;
        Alloy.createController("incidentsList", {
            params: params
        }).getView().open();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "filter";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.filter = Ti.UI.createWindow({
        layout: "vertical",
        backgroundColor: "white",
        id: "filter"
    });
    $.__views.filter && $.addTopLevelView($.__views.filter);
    $.__views.__alloyId8 = Ti.UI.createScrollView({
        id: "__alloyId8"
    });
    $.__views.filter.add($.__views.__alloyId8);
    $.__views.__alloyId9 = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        top: "10",
        width: "80%",
        left: "10%",
        id: "__alloyId9"
    });
    $.__views.__alloyId8.add($.__views.__alloyId9);
    $.__views.stateContainer = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        top: "20",
        id: "stateContainer"
    });
    $.__views.__alloyId9.add($.__views.stateContainer);
    $.__views.chkOpen = Alloy.createController("partials/checkbox", {
        id: "chkOpen",
        title: "Abiertas",
        __parentSymbol: $.__views.stateContainer
    });
    $.__views.chkOpen.setParent($.__views.stateContainer);
    $.__views.chkInProcess = Alloy.createController("partials/checkbox", {
        id: "chkInProcess",
        title: "En curso",
        __parentSymbol: $.__views.stateContainer
    });
    $.__views.chkInProcess.setParent($.__views.stateContainer);
    $.__views.chkClose = Alloy.createController("partials/checkbox", {
        id: "chkClose",
        title: "Cerradas",
        __parentSymbol: $.__views.stateContainer
    });
    $.__views.chkClose.setParent($.__views.stateContainer);
    $.__views.__alloyId10 = Alloy.createController("partials/horizontalSeparator", {
        id: "__alloyId10",
        __parentSymbol: $.__views.__alloyId9
    });
    $.__views.__alloyId10.setParent($.__views.__alloyId9);
    $.__views.dateContainer = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        top: "20",
        id: "dateContainer"
    });
    $.__views.__alloyId9.add($.__views.dateContainer);
    $.__views.chkDate = Alloy.createController("partials/checkbox", {
        id: "chkDate",
        title: "Por fechas",
        __parentSymbol: $.__views.dateContainer
    });
    $.__views.chkDate.setParent($.__views.dateContainer);
    $.__views.startDate = Alloy.createController("partials/datePicker", {
        id: "startDate",
        title: "Desde",
        __parentSymbol: $.__views.dateContainer
    });
    $.__views.startDate.setParent($.__views.dateContainer);
    $.__views.endDate = Alloy.createController("partials/datePicker", {
        id: "endDate",
        title: "Hasta",
        __parentSymbol: $.__views.dateContainer
    });
    $.__views.endDate.setParent($.__views.dateContainer);
    $.__views.__alloyId11 = Alloy.createController("partials/horizontalSeparator", {
        id: "__alloyId11",
        __parentSymbol: $.__views.__alloyId9
    });
    $.__views.__alloyId11.setParent($.__views.__alloyId9);
    $.__views.btnSearch = Ti.UI.createButton({
        top: "30dp",
        width: Ti.UI.FILL,
        right: "0",
        title: "Buscar",
        id: "btnSearch"
    });
    $.__views.__alloyId9.add($.__views.btnSearch);
    search ? $.__views.btnSearch.addEventListener("click", search) : __defers["$.__views.btnSearch!click!search"] = true;
    $.__views.__alloyId12 = Ti.UI.createView({
        layout: "vertical",
        height: "30dp",
        id: "__alloyId12"
    });
    $.__views.__alloyId9.add($.__views.__alloyId12);
    exports.destroy = function() {};
    _.extend($, $.__views);
    arguments[0] || {};
    var thisWindow = $.filter;
    var initialized = false;
    thisWindow.addEventListener("open", function() {
        init();
        Alloy.setActionbar(thisWindow, "Menú");
    });
    __defers["$.__views.btnSearch!click!search"] && $.__views.btnSearch.addEventListener("click", search);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;