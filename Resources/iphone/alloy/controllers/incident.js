function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "incident";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        var $model = __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    $.__views.incident = Ti.UI.createTableViewRow({
        id: "incident"
    });
    $.__views.incident && $.addTopLevelView($.__views.incident);
    $.__views.__alloyId13 = Ti.UI.createImageView({
        bindId: "icon",
        id: "__alloyId13"
    });
    $.__views.incident.add($.__views.__alloyId13);
    $.__views.__alloyId14 = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        id: "__alloyId14"
    });
    $.__views.incident.add($.__views.__alloyId14);
    $.__views.__alloyId15 = Ti.UI.createLabel({
        color: "#333",
        text: "undefined" != typeof $model.__transform["nombre"] ? $model.__transform["nombre"] : $model.get("nombre"),
        id: "__alloyId15"
    });
    $.__views.__alloyId14.add($.__views.__alloyId15);
    $.__views.__alloyId16 = Ti.UI.createLabel({
        color: "#333",
        text: "undefined" != typeof $model.__transform["idestado"] ? $model.__transform["idestado"] : $model.get("idestado"),
        id: "__alloyId16"
    });
    $.__views.__alloyId14.add($.__views.__alloyId16);
    exports.destroy = function() {};
    _.extend($, $.__views);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;