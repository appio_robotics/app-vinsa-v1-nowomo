function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function llamar() {
        alert("Esto es LLAMAR. No implementado para esta primera versión. ");
    }
    function arrive() {
        alert("Marcar llegada");
    }
    function map() {
        alert("Mapa. No implementado para esta primera versión");
    }
    function options() {
        alert("Opciones. No implementado para esta primera versión");
    }
    function close() {
        $.dialog.show();
    }
    function selectCloseOption(e) {
        "cerrada" === data.attributes.idestado ? alert("Esta incidencia ya está cerrada") : openPopup(e);
    }
    function openPopup(e) {
        switch (e.index.toString()) {
          case "0":
            var incidents = Alloy.Collections.incident;
            var model = incidents.get(data.attributes.id);
            model.set("idestado", "cerrada");
            model.set("type", "telefono");
            model.save();
            thisWindow.close();
            break;

          case "1":
            Alloy.createController("closeIncident", {
                modelAttributes: data.attributes
            }).getView().open();
        }
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "incidentCard";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    var __defers = {};
    Alloy.Models.instance("incident");
    $.__views.incidentCard = Ti.UI.createWindow({
        layout: "vertical",
        backgroundColor: "white",
        id: "incidentCard"
    });
    $.__views.incidentCard && $.addTopLevelView($.__views.incidentCard);
    $.__views.__alloyId19 = Ti.UI.createScrollView({
        id: "__alloyId19"
    });
    $.__views.incidentCard.add($.__views.__alloyId19);
    $.__views.__alloyId20 = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        top: "10",
        width: Ti.UI.FILL,
        left: "0",
        id: "__alloyId20"
    });
    $.__views.__alloyId19.add($.__views.__alloyId20);
    $.__views.__alloyId21 = Ti.UI.createScrollView({
        id: "__alloyId21"
    });
    $.__views.__alloyId20.add($.__views.__alloyId21);
    $.__views.infoContainer = Ti.UI.createView({
        layout: "horizontal",
        height: Ti.UI.SIZE,
        width: Ti.UI.FILL,
        id: "infoContainer"
    });
    $.__views.__alloyId21.add($.__views.infoContainer);
    $.__views.stateIcon = Ti.UI.createImageView({
        left: "5%",
        top: "0",
        width: "10%",
        id: "stateIcon"
    });
    $.__views.infoContainer.add($.__views.stateIcon);
    $.__views.textContainer = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        top: "0",
        left: "5%",
        width: "80%",
        id: "textContainer"
    });
    $.__views.infoContainer.add($.__views.textContainer);
    $.__views.firstKeyValueContainer = Ti.UI.createView({
        layout: "horizontal",
        height: Ti.UI.SIZE,
        left: "0",
        width: "95%",
        top: "0dp",
        id: "firstKeyValueContainer"
    });
    $.__views.textContainer.add($.__views.firstKeyValueContainer);
    $.__views.__alloyId22 = Ti.UI.createLabel({
        color: "#333",
        left: "0",
        width: Ti.UI.FILL,
        font: {
            fontWeight: "bold"
        },
        text: "Cliente: ",
        id: "__alloyId22"
    });
    $.__views.firstKeyValueContainer.add($.__views.__alloyId22);
    $.__views.__alloyId23 = Ti.UI.createLabel({
        color: "#333",
        width: Ti.UI.FILL,
        id: "__alloyId23"
    });
    $.__views.firstKeyValueContainer.add($.__views.__alloyId23);
    $.__views.__alloyId24 = Ti.UI.createView({
        layout: "horizontal",
        height: Ti.UI.SIZE,
        left: "0",
        width: "95%",
        top: "10dp",
        id: "__alloyId24"
    });
    $.__views.textContainer.add($.__views.__alloyId24);
    $.__views.__alloyId25 = Ti.UI.createLabel({
        color: "#333",
        left: "0",
        width: Ti.UI.FILL,
        font: {
            fontWeight: "bold"
        },
        text: "Dirección: ",
        id: "__alloyId25"
    });
    $.__views.__alloyId24.add($.__views.__alloyId25);
    $.__views.__alloyId26 = Ti.UI.createLabel({
        color: "#333",
        width: Ti.UI.FILL,
        id: "__alloyId26"
    });
    $.__views.__alloyId24.add($.__views.__alloyId26);
    $.__views.__alloyId27 = Ti.UI.createView({
        layout: "horizontal",
        height: Ti.UI.SIZE,
        left: "0",
        width: "95%",
        top: "10dp",
        id: "__alloyId27"
    });
    $.__views.textContainer.add($.__views.__alloyId27);
    $.__views.__alloyId28 = Ti.UI.createLabel({
        color: "#333",
        left: "0",
        width: Ti.UI.FILL,
        font: {
            fontWeight: "bold"
        },
        text: "Título incidencia: ",
        id: "__alloyId28"
    });
    $.__views.__alloyId27.add($.__views.__alloyId28);
    $.__views.__alloyId29 = Ti.UI.createLabel({
        color: "#333",
        width: Ti.UI.FILL,
        id: "__alloyId29"
    });
    $.__views.__alloyId27.add($.__views.__alloyId29);
    $.__views.__alloyId30 = Ti.UI.createView({
        layout: "horizontal",
        height: Ti.UI.SIZE,
        left: "0",
        width: "95%",
        top: "10dp",
        id: "__alloyId30"
    });
    $.__views.textContainer.add($.__views.__alloyId30);
    $.__views.__alloyId31 = Ti.UI.createLabel({
        color: "#333",
        left: "0",
        width: Ti.UI.FILL,
        font: {
            fontWeight: "bold"
        },
        text: "Teléfono: ",
        id: "__alloyId31"
    });
    $.__views.__alloyId30.add($.__views.__alloyId31);
    $.__views.__alloyId32 = Ti.UI.createLabel({
        color: "#333",
        width: Ti.UI.FILL,
        id: "__alloyId32"
    });
    $.__views.__alloyId30.add($.__views.__alloyId32);
    $.__views.__alloyId33 = Ti.UI.createView({
        layout: "horizontal",
        height: Ti.UI.SIZE,
        left: "0",
        width: "95%",
        top: "10dp",
        id: "__alloyId33"
    });
    $.__views.textContainer.add($.__views.__alloyId33);
    $.__views.__alloyId34 = Ti.UI.createLabel({
        color: "#333",
        left: "0",
        width: Ti.UI.FILL,
        font: {
            fontWeight: "bold"
        },
        text: "Detalle: ",
        id: "__alloyId34"
    });
    $.__views.__alloyId33.add($.__views.__alloyId34);
    $.__views.__alloyId35 = Ti.UI.createLabel({
        color: "#333",
        width: Ti.UI.FILL,
        id: "__alloyId35"
    });
    $.__views.__alloyId33.add($.__views.__alloyId35);
    $.__views.__alloyId36 = Alloy.createController("partials/horizontalSeparator", {
        id: "__alloyId36",
        __parentSymbol: $.__views.__alloyId20
    });
    $.__views.__alloyId36.setParent($.__views.__alloyId20);
    $.__views.buttonsContainer = Ti.UI.createView({
        layout: "horizontal",
        height: Ti.UI.SIZE,
        top: "30",
        id: "buttonsContainer"
    });
    $.__views.__alloyId20.add($.__views.buttonsContainer);
    $.__views.__alloyId37 = Alloy.createController("partials/iconButton", {
        title: "Llamar",
        image: "call.png",
        id: "__alloyId37",
        __parentSymbol: $.__views.buttonsContainer
    });
    $.__views.__alloyId37.setParent($.__views.buttonsContainer);
    llamar ? $.__views.__alloyId37.on("click", llamar) : __defers["$.__views.__alloyId37!click!llamar"] = true;
    $.__views.__alloyId38 = Alloy.createController("partials/iconButton", {
        title: "Marcar llegada",
        image: "arrive.png",
        id: "__alloyId38",
        __parentSymbol: $.__views.buttonsContainer
    });
    $.__views.__alloyId38.setParent($.__views.buttonsContainer);
    arrive ? $.__views.__alloyId38.on("click", arrive) : __defers["$.__views.__alloyId38!click!arrive"] = true;
    $.__views.__alloyId39 = Alloy.createController("partials/iconButton", {
        title: "Mapa",
        image: "map.png",
        id: "__alloyId39",
        __parentSymbol: $.__views.buttonsContainer
    });
    $.__views.__alloyId39.setParent($.__views.buttonsContainer);
    map ? $.__views.__alloyId39.on("click", map) : __defers["$.__views.__alloyId39!click!map"] = true;
    $.__views.__alloyId40 = Alloy.createController("partials/iconButton", {
        title: "Opciones",
        image: "options.png",
        id: "__alloyId40",
        __parentSymbol: $.__views.buttonsContainer
    });
    $.__views.__alloyId40.setParent($.__views.buttonsContainer);
    options ? $.__views.__alloyId40.on("click", options) : __defers["$.__views.__alloyId40!click!options"] = true;
    $.__views.__alloyId41 = Alloy.createController("partials/iconButton", {
        title: "Cerrar incidencia",
        image: "close.png",
        id: "__alloyId41",
        __parentSymbol: $.__views.buttonsContainer
    });
    $.__views.__alloyId41.setParent($.__views.buttonsContainer);
    close ? $.__views.__alloyId41.on("click", close) : __defers["$.__views.__alloyId41!click!close"] = true;
    $.__views.__alloyId42 = Ti.UI.createView({
        layout: "vertical",
        height: "60dp",
        id: "__alloyId42"
    });
    $.__views.buttonsContainer.add($.__views.__alloyId42);
    var __alloyId44 = [];
    __alloyId44.push("Por teléfono");
    __alloyId44.push("Presencial");
    $.__views.dialog = Ti.UI.createOptionDialog({
        options: __alloyId44,
        id: "dialog",
        title: "Marcar llegada"
    });
    selectCloseOption ? $.__views.dialog.addEventListener("click", selectCloseOption) : __defers["$.__views.dialog!click!selectCloseOption"] = true;
    $.__views.__alloyId47 = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        id: "__alloyId47"
    });
    $.__views.__alloyId47 && $.addTopLevelView($.__views.__alloyId47);
    var __alloyId48 = function() {
        $.stateIcon.image = _.isFunction(Alloy.Models.incident.transform) ? Alloy.Models.incident.transform()["iconUrl"] : Alloy.Models.incident.get("iconUrl");
        $.stateIcon.image = _.isFunction(Alloy.Models.incident.transform) ? Alloy.Models.incident.transform()["iconUrl"] : Alloy.Models.incident.get("iconUrl");
        $.__alloyId23.text = _.isFunction(Alloy.Models.incident.transform) ? Alloy.Models.incident.transform()["clienteNombre"] : Alloy.Models.incident.get("clienteNombre");
        $.__alloyId23.text = _.isFunction(Alloy.Models.incident.transform) ? Alloy.Models.incident.transform()["clienteNombre"] : Alloy.Models.incident.get("clienteNombre");
        $.__alloyId26.text = _.isFunction(Alloy.Models.incident.transform) ? Alloy.Models.incident.transform()["ctDireccion"] : Alloy.Models.incident.get("ctDireccion");
        $.__alloyId26.text = _.isFunction(Alloy.Models.incident.transform) ? Alloy.Models.incident.transform()["ctDireccion"] : Alloy.Models.incident.get("ctDireccion");
        $.__alloyId29.text = _.isFunction(Alloy.Models.incident.transform) ? Alloy.Models.incident.transform()["nombre"] : Alloy.Models.incident.get("nombre");
        $.__alloyId29.text = _.isFunction(Alloy.Models.incident.transform) ? Alloy.Models.incident.transform()["nombre"] : Alloy.Models.incident.get("nombre");
        $.__alloyId32.text = _.isFunction(Alloy.Models.incident.transform) ? Alloy.Models.incident.transform()["ctTelefono"] : Alloy.Models.incident.get("ctTelefono");
        $.__alloyId32.text = _.isFunction(Alloy.Models.incident.transform) ? Alloy.Models.incident.transform()["ctTelefono"] : Alloy.Models.incident.get("ctTelefono");
        $.__alloyId35.text = _.isFunction(Alloy.Models.incident.transform) ? Alloy.Models.incident.transform()["descripcion_incidencia"] : Alloy.Models.incident.get("descripcion_incidencia");
        $.__alloyId35.text = _.isFunction(Alloy.Models.incident.transform) ? Alloy.Models.incident.transform()["descripcion_incidencia"] : Alloy.Models.incident.get("descripcion_incidencia");
    };
    Alloy.Models.incident.on("fetch change destroy", __alloyId48);
    exports.destroy = function() {
        Alloy.Models.incident.off("fetch change destroy", __alloyId48);
    };
    _.extend($, $.__views);
    require("/dev/Dev");
    var args = arguments[0] || {};
    var data = args.data;
    var incident = Alloy.Models.instance("incident");
    switch (data.attributes.idestado) {
      case "abierta":
        data.attributes.iconUrl = "/images/icons/circleRed.png";
        break;

      case "curso":
        data.attributes.iconUrl = "/images/icons/circleOrange.png";
        break;

      case "cerrada":
        data.attributes.iconUrl = "/images/icons/circleGreen.png";
    }
    var clearAttributes = {
        iconUrl: false
    };
    incident.set(clearAttributes);
    incident.set(data.attributes);
    var thisWindow = $.incidentCard;
    thisWindow.addEventListener("open", function() {
        Alloy.setActionbar(thisWindow, "Listado de incidencias");
    });
    thisWindow.addEventListener("close", function() {
        $.destroy();
    });
    __defers["$.__views.__alloyId37!click!llamar"] && $.__views.__alloyId37.on("click", llamar);
    __defers["$.__views.__alloyId38!click!arrive"] && $.__views.__alloyId38.on("click", arrive);
    __defers["$.__views.__alloyId39!click!map"] && $.__views.__alloyId39.on("click", map);
    __defers["$.__views.__alloyId40!click!options"] && $.__views.__alloyId40.on("click", options);
    __defers["$.__views.__alloyId41!click!close"] && $.__views.__alloyId41.on("click", close);
    __defers["$.__views.dialog!click!selectCloseOption"] && $.__views.dialog.addEventListener("click", selectCloseOption);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;