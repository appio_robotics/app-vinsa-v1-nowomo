function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function updateTimeSlider(e) {
        var value = Math.floor(e.value);
        $.timeLabel.text = "Tiempo de desplazamiento: " + value + " minutos";
    }
    function updateClientTimeSlider(e) {
        var value = Math.floor(e.value);
        $.clientTimeLabel.text = "Tiempo en cliente: " + value + " minutos";
    }
    function updateDistanceSlider(e) {
        var value = Math.floor(e.value);
        $.distanceLabel.text = "Distancia recorrida: " + value + " Km.";
    }
    function saveAndSend() {
        var model = incidents.get(modelAttributes.id);
        var funciona = $.chkWorks.getView("checkboxElement").value ? 1 : 0;
        var facturable = $.chkBillable.getView("checkboxElement").value ? 1 : 0;
        var garantia = $.chkWarranty.getView("checkboxElement").value ? 1 : 0;
        if ($.chkPending.getView("checkboxElement").value) var pendiente = $.picker.getSelectedRow(0).title; else var pendiente = "";
        var tiempodesplazamiento = Math.floor($.timeSlider.value);
        var tiempoencliente = Math.floor($.clientTimeSlider.value);
        var distancia = Math.floor($.distanceSlider.value);
        var detalles = $.details.value;
        model.set("idestado", "cerrada");
        model.set("funciona", funciona);
        model.set("facturable", facturable);
        model.set("garantia", garantia);
        model.set("pendiente", pendiente);
        model.set("tiempodesplazamiento", tiempodesplazamiento);
        model.set("tiempoencliente", tiempoencliente);
        model.set("distancia", distancia);
        model.set("detalles", detalles);
        model.save();
        thisWindow.close();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "closeIncident";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.closeIncident = Ti.UI.createWindow({
        layout: "vertical",
        backgroundColor: "white",
        id: "closeIncident"
    });
    $.__views.closeIncident && $.addTopLevelView($.__views.closeIncident);
    $.__views.__alloyId0 = Ti.UI.createScrollView({
        id: "__alloyId0"
    });
    $.__views.closeIncident.add($.__views.__alloyId0);
    $.__views.__alloyId1 = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        top: "10",
        width: "80%",
        left: "10%",
        id: "__alloyId1"
    });
    $.__views.__alloyId0.add($.__views.__alloyId1);
    $.__views.chkWorks = Alloy.createController("partials/checkbox", {
        id: "chkWorks",
        title: "Funciona",
        __parentSymbol: $.__views.__alloyId1
    });
    $.__views.chkWorks.setParent($.__views.__alloyId1);
    $.__views.chkBillable = Alloy.createController("partials/checkbox", {
        id: "chkBillable",
        title: "Facturable",
        __parentSymbol: $.__views.__alloyId1
    });
    $.__views.chkBillable.setParent($.__views.__alloyId1);
    $.__views.chkWarranty = Alloy.createController("partials/checkbox", {
        id: "chkWarranty",
        title: "Garantía",
        __parentSymbol: $.__views.__alloyId1
    });
    $.__views.chkWarranty.setParent($.__views.__alloyId1);
    $.__views.__alloyId2 = Alloy.createController("partials/horizontalSeparator", {
        id: "__alloyId2",
        __parentSymbol: $.__views.__alloyId1
    });
    $.__views.__alloyId2.setParent($.__views.__alloyId1);
    $.__views.chkPending = Alloy.createController("partials/checkbox", {
        id: "chkPending",
        title: "Pendiente",
        __parentSymbol: $.__views.__alloyId1
    });
    $.__views.chkPending.setParent($.__views.__alloyId1);
    $.__views.picker = Ti.UI.createPicker({
        width: Ti.UI.FILL,
        id: "picker",
        selectionIndicator: "true"
    });
    $.__views.__alloyId1.add($.__views.picker);
    var __alloyId3 = [];
    $.__views.column1 = Ti.UI.createPickerColumn({
        id: "column1"
    });
    __alloyId3.push($.__views.column1);
    $.__views.__alloyId4 = Ti.UI.createPickerRow({
        title: "Reparación de equipos",
        id: "__alloyId4"
    });
    $.__views.column1.addRow($.__views.__alloyId4);
    $.__views.__alloyId5 = Ti.UI.createPickerRow({
        title: "Otros",
        id: "__alloyId5"
    });
    $.__views.column1.addRow($.__views.__alloyId5);
    $.__views.picker.add(__alloyId3);
    $.__views.__alloyId6 = Alloy.createController("partials/horizontalSeparator", {
        id: "__alloyId6",
        __parentSymbol: $.__views.__alloyId1
    });
    $.__views.__alloyId6.setParent($.__views.__alloyId1);
    $.__views.distanceLabel = Ti.UI.createLabel({
        color: "#333",
        top: "30dp",
        left: "0dp",
        text: "Distancia recorrida",
        id: "distanceLabel"
    });
    $.__views.__alloyId1.add($.__views.distanceLabel);
    $.__views.distanceSlider = Ti.UI.createSlider({
        width: Ti.UI.FILL,
        id: "distanceSlider",
        min: "0",
        max: "300",
        value: "0"
    });
    $.__views.__alloyId1.add($.__views.distanceSlider);
    updateDistanceSlider ? $.__views.distanceSlider.addEventListener("change", updateDistanceSlider) : __defers["$.__views.distanceSlider!change!updateDistanceSlider"] = true;
    $.__views.timeLabel = Ti.UI.createLabel({
        color: "#333",
        top: "30dp",
        left: "0dp",
        text: "Tiempo de desplazamiento",
        id: "timeLabel"
    });
    $.__views.__alloyId1.add($.__views.timeLabel);
    $.__views.timeSlider = Ti.UI.createSlider({
        width: Ti.UI.FILL,
        id: "timeSlider",
        min: "0",
        max: "480",
        value: "0"
    });
    $.__views.__alloyId1.add($.__views.timeSlider);
    updateTimeSlider ? $.__views.timeSlider.addEventListener("change", updateTimeSlider) : __defers["$.__views.timeSlider!change!updateTimeSlider"] = true;
    $.__views.clientTimeLabel = Ti.UI.createLabel({
        color: "#333",
        top: "30dp",
        left: "0dp",
        text: "Tiempo en el cliente",
        id: "clientTimeLabel"
    });
    $.__views.__alloyId1.add($.__views.clientTimeLabel);
    $.__views.clientTimeSlider = Ti.UI.createSlider({
        width: Ti.UI.FILL,
        id: "clientTimeSlider",
        min: "0",
        max: "480",
        value: "0"
    });
    $.__views.__alloyId1.add($.__views.clientTimeSlider);
    updateClientTimeSlider ? $.__views.clientTimeSlider.addEventListener("change", updateClientTimeSlider) : __defers["$.__views.clientTimeSlider!change!updateClientTimeSlider"] = true;
    $.__views.details = Ti.UI.createTextArea({
        width: Ti.UI.FILL,
        top: "30dp",
        id: "details",
        hintText: "Detalles"
    });
    $.__views.__alloyId1.add($.__views.details);
    $.__views.__alloyId7 = Alloy.createController("partials/horizontalSeparator", {
        id: "__alloyId7",
        __parentSymbol: $.__views.__alloyId1
    });
    $.__views.__alloyId7.setParent($.__views.__alloyId1);
    $.__views.btnSearch = Ti.UI.createButton({
        top: "30dp",
        width: Ti.UI.FILL,
        title: "Guardar y enviar",
        id: "btnSearch"
    });
    $.__views.__alloyId1.add($.__views.btnSearch);
    saveAndSend ? $.__views.btnSearch.addEventListener("click", saveAndSend) : __defers["$.__views.btnSearch!click!saveAndSend"] = true;
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    var modelAttributes = args.modelAttributes;
    var thisWindow = $.closeIncident;
    var incidents = Alloy.Collections.incident;
    updateTimeSlider({
        value: $.timeSlider.value
    });
    updateClientTimeSlider({
        value: $.clientTimeSlider.value
    });
    updateDistanceSlider({
        value: $.distanceSlider.value
    });
    thisWindow.addEventListener("open", function() {
        Alloy.setActionbar(thisWindow, "Incidencia");
    });
    __defers["$.__views.distanceSlider!change!updateDistanceSlider"] && $.__views.distanceSlider.addEventListener("change", updateDistanceSlider);
    __defers["$.__views.timeSlider!change!updateTimeSlider"] && $.__views.timeSlider.addEventListener("change", updateTimeSlider);
    __defers["$.__views.clientTimeSlider!change!updateClientTimeSlider"] && $.__views.clientTimeSlider.addEventListener("change", updateClientTimeSlider);
    __defers["$.__views.btnSearch!click!saveAndSend"] && $.__views.btnSearch.addEventListener("click", saveAndSend);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;