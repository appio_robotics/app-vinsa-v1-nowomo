function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function init() {
        $.user.blur();
        $.user.addEventListener("focus", function() {
            if (!initialized) {
                $.user.blur();
                initialized = true;
            }
        });
        var userData = LoginManager.getUserDataIfRememberMe();
        if (userData) {
            $.user.value = userData["userName"];
            $.psw.value = userData["passwordInForm"];
            $.rememberMe.getView("checkboxElement").value = userData["rememberMe"];
        }
    }
    function login() {
        var rememberMe = "undefined" != typeof $.rememberMe.getView("checkboxElement").value ? $.rememberMe.getView("checkboxElement").value : false;
        LoginManager.checkLogin({
            user: $.user.value,
            psw: $.psw.value,
            rememberMe: rememberMe,
            success: function() {
                Alloy.createController("main").getView().open();
            },
            error: function() {
                alert("Login incorrecto");
            }
        });
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "login";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.login = Ti.UI.createWindow({
        layout: "vertical",
        backgroundColor: "white",
        navBarHidden: "true",
        id: "login"
    });
    $.__views.login && $.addTopLevelView($.__views.login);
    $.__views.__alloyId67 = Ti.UI.createScrollView({
        id: "__alloyId67"
    });
    $.__views.login.add($.__views.__alloyId67);
    $.__views.mainContainer = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        width: "80%",
        left: "10%",
        top: "10%",
        id: "mainContainer"
    });
    $.__views.__alloyId67.add($.__views.mainContainer);
    $.__views.logo = Ti.UI.createImageView({
        height: Ti.UI.SIZE,
        width: Ti.UI.SIZE,
        id: "logo",
        image: "/images/logo.png"
    });
    $.__views.mainContainer.add($.__views.logo);
    $.__views.title = Ti.UI.createLabel({
        color: "#333",
        textAlign: Titanium.UI.TEXT_ALIGNMENT_CENTER,
        top: "30dp",
        text: "SISTEMA DE GESTIÓN DE INCIDENCIAS 1.0",
        id: "title"
    });
    $.__views.mainContainer.add($.__views.title);
    $.__views.formContainer = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        top: "10dp",
        id: "formContainer"
    });
    $.__views.mainContainer.add($.__views.formContainer);
    $.__views.user = Ti.UI.createTextField({
        width: Ti.UI.FILL,
        top: "20dp",
        id: "user",
        hintText: "Usuario"
    });
    $.__views.formContainer.add($.__views.user);
    $.__views.psw = Ti.UI.createTextField({
        width: Ti.UI.FILL,
        top: "20dp",
        id: "psw",
        passwordMask: "true",
        hintText: "Contraseña"
    });
    $.__views.formContainer.add($.__views.psw);
    $.__views.rememberMe = Alloy.createController("partials/checkbox", {
        left: "30",
        top: "30dp",
        id: "rememberMe",
        title: "Recordarme en este equipo",
        __parentSymbol: $.__views.formContainer
    });
    $.__views.rememberMe.setParent($.__views.formContainer);
    $.__views.btnLogin = Ti.UI.createButton({
        top: "10dp",
        width: Ti.UI.FILL,
        right: "0",
        height: Ti.UI.SIZE,
        title: "Entrar",
        id: "btnLogin"
    });
    $.__views.formContainer.add($.__views.btnLogin);
    login ? $.__views.btnLogin.addEventListener("click", login) : __defers["$.__views.btnLogin!click!login"] = true;
    $.__views.__alloyId68 = Ti.UI.createView({
        layout: "vertical",
        height: "60dp",
        id: "__alloyId68"
    });
    $.__views.formContainer.add($.__views.__alloyId68);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var initialized = false;
    var LoginManager = require("/loginManager");
    require("/dev/Dev");
    $.login.addEventListener("open", function() {
        init();
    });
    Ti.UI.Android.hideSoftKeyboard();
    $.user.blur();
    __defers["$.__views.btnLogin!click!login"] && $.__views.btnLogin.addEventListener("click", login);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;