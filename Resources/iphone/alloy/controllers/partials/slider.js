function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "partials/slider";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    $.__views.sliderContainer = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        top: "20dp",
        id: "sliderContainer"
    });
    $.__views.sliderContainer && $.addTopLevelView($.__views.sliderContainer);
    $.__views.label = Ti.UI.createLabel({
        color: "#333",
        id: "label"
    });
    $.__views.sliderContainer.add($.__views.label);
    $.__views.slider = Ti.UI.createSlider({
        id: "slider",
        min: "0",
        max: "480",
        value: "23"
    });
    $.__views.sliderContainer.add($.__views.slider);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    $.label.text = args.title;
    $.slider.min = args.min;
    $.slider.max = args.max;
    $.slider.value = args.value;
    $.slider.id = args.sliderId;
    $.label.id = args.labelId;
    alert("al slider le di id " + args.sliderId + " y quedó " + $.slider.id);
    $.slider.addEventListener("change", function(e) {
        var value = e.value;
        $.distanceLabel.text = text1 + value + text2;
    });
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;