function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "partials/datePicker";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    $.__views.datePickerContainer = Ti.UI.createView({
        layout: "horizontal",
        height: Ti.UI.SIZE,
        width: Ti.UI.FILL,
        top: "20",
        id: "datePickerContainer"
    });
    $.__views.datePickerContainer && $.addTopLevelView($.__views.datePickerContainer);
    $.__views.dateTextField = Ti.UI.createTextField({
        width: "80%",
        id: "dateTextField"
    });
    $.__views.datePickerContainer.add($.__views.dateTextField);
    $.__views.__alloyId77 = Ti.UI.createImageView({
        width: "15%",
        left: "5%",
        image: "/images/icons/calendar.png",
        id: "__alloyId77"
    });
    $.__views.datePickerContainer.add($.__views.__alloyId77);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    $.dateTextField.hintText = args.title;
    $.datePickerContainer.addEventListener("click", function() {
        var picker = Ti.UI.createPicker({
            type: Ti.UI.PICKER_TYPE_DATE
        });
        picker.showDatePickerDialog({
            callback: function(e) {
                if (!e.cancel) {
                    var dateValue = e.value;
                    $.dateTextField.value = dateValue.getMonth() + 1 + "/" + dateValue.getDate() + "/" + dateValue.getFullYear();
                    $.dateTextField.blur();
                }
            }
        });
    });
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;