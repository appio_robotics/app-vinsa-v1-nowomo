function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "partials/timePicker";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    $.__views.timePicker = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        id: "timePicker"
    });
    $.__views.timePicker && $.addTopLevelView($.__views.timePicker);
    exports.destroy = function() {};
    _.extend($, $.__views);
    arguments[0] || {};
    $.timeTextField.addEventListener("click", function() {
        var picker = Ti.UI.createPicker({
            type: Ti.UI.PICKER_TYPE_TIME
        });
        picker.showTimePickerDialog({
            callback: function(e) {
                var time = e.value;
                $.timeTextField.value = time.getHours() + ":" + time.getMinutes();
                $.timeTextField.blur();
            }
        });
    });
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;