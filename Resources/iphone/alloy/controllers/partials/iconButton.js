function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "partials/iconButton";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    $.__views.iconContainer = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        width: "33%",
        id: "iconContainer"
    });
    $.__views.iconContainer && $.addTopLevelView($.__views.iconContainer);
    $.__views.icon = Ti.UI.createImageView({
        top: "10%",
        width: "75%",
        height: Ti.UI.SIZE,
        id: "icon"
    });
    $.__views.iconContainer.add($.__views.icon);
    $.__views.label = Ti.UI.createLabel({
        color: "#333",
        top: "10",
        height: Ti.UI.SIZE,
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        id: "label"
    });
    $.__views.iconContainer.add($.__views.label);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    $.icon.image = "/images/icons/" + args.image;
    $.label.text = args.title;
    $.iconContainer.addEventListener("click", function(e) {
        $.trigger("click", e);
    });
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;