function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "menuIcon";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.iconClients = Ti.UI.createView({
        id: "iconClients"
    });
    $.__views.iconClients && $.addTopLevelView($.__views.iconClients);
    $.__views.__alloyId8 = Ti.UI.createImageView({
        image: "/images/menuIcons/clients.png",
        id: "__alloyId8"
    });
    $.__views.iconClients.add($.__views.__alloyId8);
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.iconClients.addEventListener("click", function(e) {
        $.trigger("click", e);
    });
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;