var Cloud = require("/ti.cloud"), CloudPush = require("/ti.cloudpush");

var deviceToken, acsUser = "vinsa", acsPassword = "vinsa44xda73";

CloudPush.debug = true;

CloudPush.enabled = true;

CloudPush.showTrayNotificationsWhenFocused = true;

CloudPush.focusAppOnPush = false;

CloudPush.singleCallback = true;

exports.subscribe = function() {
    if (!Ti.Network.online) return;
    var currentUser = require("/DataModel").currentUser.get();
    var userId = currentUser["id"];
    Cloud.Users.login({
        login: acsUser,
        password: acsPassword
    }, function(e) {
        e.success ? CloudPush.retrieveDeviceToken({
            success: function(e) {
                deviceToken = e.deviceToken;
                Cloud.PushNotifications.unsubscribe({
                    device_token: deviceToken
                }, function() {
                    Cloud.PushNotifications.subscribe({
                        channel: "user_" + userId,
                        device_token: deviceToken,
                        type: "android"
                    }, function(e) {
                        e.success ? CloudPush.addEventListener("callback", function() {}) : require("/dev/Dev").isDev && alert("Error in subscribe:" + (e.error && e.message || JSON.stringify(e)));
                    });
                });
            },
            error: function(e) {
                require("/dev/Dev").isDev && alert("Failed to register for push! " + e.error);
            }
        }) : require("/dev/Dev").isDev && alert("Error en registro de push: " + (e.error && e.message || JSON.stringify(e)));
    });
};